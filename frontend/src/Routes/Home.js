import React, { Component } from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import LoginPanel from '../Components/LoginPanel/LoginPanel'
import Products from '../Components/AdminPanel/Products/Products'
import Orders from '../Components/AdminPanel/Orders/Orders'
import Screens from '../Components/AdminPanel/Screens/Screens'
import Employees from '../Components/AdminPanel/Employees/Employees'
import Charts from  '../Components/AdminPanel/Charts/Charts'
import MainAdmin from '../Components/AdminPanel/MainAdmin/MainAdmin'
import MainUser from '../Components/UserPanel/MainUser/MainUser'
import MainEmployee from '../Components/EmployeePanel/MainEmployee/MainEmployee'

export default class Home extends Component {
  state ={
    isMenu: true
  }

  menuShow = () =>{
    this.setState({
      isMenu: !this.state.isMenu
    })
  }

  render(){
    return(
      <BrowserRouter>
        <div>
          <Route path="/" exact render={() => 
              <LoginPanel />
          } />
          <Route path="/admin" render={()=> 
            <MainAdmin 
              isMenu={this.state.isMenu}
              menuShow={()=>this.menuShow()}
              />
          }/>
          <Route path="/user" render={()=> 
              <MainUser />
          }/>
          <Route path="/employee" render={()=> 
              <MainEmployee />
          }/>
          <Route path="/admin/employees" render={() => <Employees isMenu={this.state.isMenu} />} />
          <Route path="/admin/orders" render={() => <Orders isMenu={this.state.isMenu} />} />
          <Route path="/admin/products" render={() => <Products isMenu={this.state.isMenu} />} />
          <Route path="/admin/screens" render={() => <Screens isMenu={this.state.isMenu} />} />
          <Route path="/admin/charts" render={() => <Charts isMenu={this.state.isMenu} />} />
        </div>
      </BrowserRouter>
    )
  }
}

