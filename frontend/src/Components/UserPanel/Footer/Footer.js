import React from 'react'
import './MainMenu.scss'
// import './MainMenu.css'
import {
    Container,
    Divider,
    Grid,
    Header,
    Image,
    List,
    Segment,
    Icon,
} from 'semantic-ui-react'
import avatarImage from '../../../img/malaPostac.png'


const Footer = () => (

    <div className="GradientStyle">
        <Segment inverted vertical style={{ margin: '5em 0em 0em', padding: '5em 0em', backgroundColor: '#48b4db' }}>
            <Container textAlign='center'>
                <Grid divided inverted stackable>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='Information about project' />
                            <List link inverted>
                                <Icon name="gitlab"></Icon>
                                <List.Item><a href="https://gitlab.com/Szumigalski/tas-computer-hardware-warehouse">Repository</a></List.Item>
                            </List>
                        </Grid.Column>

                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='Technology' />
                            <List link inverted>

                                <List.Item><a href="https://docs.microsoft.com/pl-pl/aspnet/core/release-notes/aspnetcore-2.1?view=aspnetcore-2.2">Why .Net Core 2.1?</a></List.Item>
                                <List.Item><a href="https://reactjs.org/">Frontend - React</a></List.Item>
                                <List.Item><a href="https://react.semantic-ui.com">UI design</a></List.Item>
                            </List>
                        </Grid.Column>

                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='Footer Header' />
                            <p>
                                Powered By The best TAS Specialist Team © 2018/2019</p>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

                <Divider inverted section />

                <Image centred src={avatarImage} alt='avatar' avatar />
                <List horizontal inverted divided link>
                    <List.Item><a href="https://pl.linkedin.com/in/natalia-gawron-270292173">Natalia Gawron</a></List.Item>
                    <List.Item><a href="https://www.linkedin.com/in/mikołaj-szumigalski-086a13160/" >Mikołaj Szumigalski</a></List.Item>
                    <List.Item><a href="https://www.linkedin.com/in/łukasz-góreczny-145140172/">Łukasz Góreczny</a></List.Item>
                    <List.Item><a href="https://www.linkedin.com/in/karol-górzyński-a8a62317a/">Karol Górzyński</a></List.Item>
                    <List.Item>Marek Garniec</List.Item>
                </List>
            </Container>
        </Segment>
    </div >
)

export default Footer;