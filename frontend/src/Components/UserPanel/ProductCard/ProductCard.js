import React, { Component } from 'react'
import { Card, Icon, Image, Label, Button, Grid } from 'semantic-ui-react'
import PropTypes from 'prop-types'

export default class ProductCard extends Component {
    render() {
        return (
            <Grid>
                <Card style={{ marginBottom: '20px', marginTop: '20px' }}>
                    <Image src={this.props.image} />
                    <Card.Content style={{ backgroundColor: '#b4e6f8' }}>
                        <Card.Header style={{margin: 'auto'}}>{this.props.productName}</Card.Header>
                        <Card.Meta>{this.props.productCategory}</Card.Meta>
                        <Card.Description>Liczba sztuk w magazynie: {this.props.productQuanity}</Card.Description>
                    </Card.Content>
                    <Card.Content extra style={{ backgroundColor: '#96cbdf' }}>
                        <Label style={{ backgroundColor: 'lila' }} as='a' tag>{this.props.productPrice} PLN</Label>
                        <Button animated='vertical' secondary style={{ left: 30 }}>
                            <Button.Content hidden>Zamów</Button.Content>
                            <Button.Content visible>
                                <Icon name='shop' />
                            </Button.Content>
                        </Button>
                    </Card.Content>
                </Card>
            </Grid>
        )
    }
}
ProductCard.propTypes = {
    image: PropTypes.string,
    productName: PropTypes.string,
    productCategory: PropTypes.string,
    productPrice: PropTypes.number,
    productQuanity: PropTypes.number
};