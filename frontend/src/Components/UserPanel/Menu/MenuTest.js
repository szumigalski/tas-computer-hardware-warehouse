import React, { Component } from 'react'
import { Menu, Dropdown, Button, Label, Icon } from 'semantic-ui-react'
import axios from 'axios';

export default class MenuTest extends Component {
    state = {
        categoryList: [],
        selected: "",
        validationError: ""
    }
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
            categoryList: [],
            isLoading: true
        };

        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.categoryChose = this.categoryChose.bind(this);
    }

    componentWillMount() {
        this.getCategories()
    }

    getCategories() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/category`)
            .then(function (response) {
                let categories = []
                response.data.map(category => {
                    return categories.push({ id: category.id, value: category.name, text: category.name, parent: category.parentId })
                })
                self.setState({
                    categoryList: categories,
                    isLoading: false
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({ showMenu: true }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    closeMenu(event) {

        if (!this.dropdownMenu.contains(event.target)) {

            this.setState({ showMenu: false }, () => {
                document.removeEventListener('click', this.closeMenu);
            });

        }
    }
    renderButton = () => {
        let table = [];
        if (this.state.isLoading === false) {
            for (var i = 0; i < this.state.categoryList.length; i++) {
                let parent = this.state.categoryList[i].parent;
                if (parent === null) {
                    let anyChilds = this.checkForChilds(this.state.categoryList[i].id);
                    if (anyChilds === 1) {
                        let name = this.state.categoryList[i].value;
                        let index = this.state.categoryList[i].id;
                        table.push(<Dropdown direction="left" pointing className='link item' item text={name} style={{
                            paddingTop: 20,
                            paddingBottom: 20,
                            fontFamily: "'Arimo', cursive",
                            fontWeight: '900',
                            color: '#fff',
                            marginRight: 'auto',
                            textAlign: 'center',
                            backgroundColor: '#68a0cf',
                            borderRadius: 20,


                        }}><Dropdown.Menu>{this.getChilds(index, 0)}</Dropdown.Menu></Dropdown>)
                    }
                    else {
                        let name = this.state.categoryList[i].value;
                        table.push(<Dropdown id={this.state.categoryList[i].id} onClick={this.categoryChose.bind(this)} className='link item' item text={name} style={{
                            paddingTop: 20,
                            paddingBottom: 20,
                            fontFamily: "'Arimo', cursive",
                            fontWeight: '900',
                            color: '#fff',
                            marginRight: 'auto',
                            textAlign: 'center',
                            backgroundColor: '#68a0cf',
                            borderRadius: 20,
                        }}></Dropdown>)
                    }
                }
            }
            return table;
        }
    }

    checkForChilds = (id) => {

        for (var i = 0; i < this.state.categoryList.length; i++) {
            if (this.state.categoryList[i].parent === id) {
                return 1;
            }
        }
        return 0;
    }

    getChilds = (index, linia) => {
        let table = []
        let haveChild = false
        for (var i = linia + 1; i < this.state.categoryList.length; i++) {
            let line = this.state.categoryList[i];
            if (line.parent === index) {
                for (var j = i + 1; j < this.state.categoryList.length - 1; j++) {
                    if (line.id === this.state.categoryList[j].parent) {
                        haveChild = true;
                        break;
                    }
                }
                if (haveChild === false) {
                    table.push(<Dropdown.Item id={line.id} onClick={this.categoryChose}>{line.value}</Dropdown.Item>)
                }
                else {
                    table.push(<Dropdown pointing className='link item' item text={line.text}><Dropdown.Menu>{this.getChilds(line.id, i)}</Dropdown.Menu></Dropdown>)
                    haveChild = false;
                }
            }
        }
        return table;
    }

    categoryChose = (e) => {
        if (e.target.id === "") {
            for (var i = 0; i < this.state.categoryList.length; i++) {
                if (this.state.categoryList[i].text === e.target.textContent) {
                    debugger
                    this.props.triggerCategoryRefresh(this.state.categoryList[i].id);
                }
            }
        }
        else {
            this.props.triggerCategoryRefresh(e.target.id);
        }
    }

    render() {
        return (
            <div>
                <Menu stackable style={{
                    backgroundColor: '#68a0cf',
                }}>
                    {this.renderButton()}
                </Menu>
                <Button as='div' labelPosition='right' onClick={this.props.deleteFilters}>
      <Button color='yellow'>
        <Icon name='filter' />
        
      </Button>
      <Label as='a' basic color='red' pointing='left'>
        Usuń filtr
      </Label>
    </Button>
            </div >
        );
    }
}