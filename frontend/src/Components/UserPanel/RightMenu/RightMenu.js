import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'
import { withRouter } from "react-router-dom"
import './RightMenu.css';

class RightMenu extends Component {
    handleItemClick = (name) => {
        debugger
        this.setState({ activeItem: name.currentTarget.id },
            this.setHistory(name.currentTarget.id)
        )
    }

    setHistory(name) {
        if (name === 'home') {
            this.props.history.push("/")
        }
    }



    render() {
        return (
            <SideNav
                onSelect={(selected) => {
                }}>
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected="myaccount">
                    <NavItem eventKey="myaccount" onClick={this.props.onUserAccount}>
                        <NavIcon >
                            <Icon name='user circle' size='large' />
                        </NavIcon>
                        <NavText>My account
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="Basket" onClick={this.props.onBasket}>
                        <NavIcon >
                            <Icon name='shopping basket circle' size='large' />
                        </NavIcon>
                        <NavText>My basket
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="home" id="home" onClick={this.handleItemClick}>
                        <NavIcon>
                            <Icon name='sign-out' size='large' />
                        </NavIcon>
                        <NavText>
                            Sign out
                        </NavText>
                    </NavItem>
                </SideNav.Nav>
            </SideNav >
        )
    }
}
export default withRouter(RightMenu)