import React, { Component } from 'react'
import { Segment, Card, Icon, Image, Label, Button } from 'semantic-ui-react'
import image1 from '../../../img/notFound.jpg'
import axios from 'axios';
import { SemanticToastContainer, toast } from 'react-semantic-toasts'
import './Product.css';

export default class Products extends Component {
    state = {
        productsListOriginal: [],
        productList: [],
        selected: "",
        validationError: ""
    }
    constructor() {
        super();

        this.state = {
            showMenu: false,
            productList: [],
            isLoading: true
        };
        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    componentWillMount() {

        this.getProducts()
    }



    getProducts() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/product`)
            .then(function (response) {
                let products = []
                response.data.map(product => {
                    return products.push({ id: product.id, text: product.name, quantity: product.quantity, price: product.price, imageLink: product.imageLink, categoryId: product.categoryId, tagId: product.tagId })
                })
                self.setState({
                    productsListOriginal: products,
                    productList: products,
                    isLoading: false
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({ showMenu: true }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    closeMenu(event) {

        if (!this.dropdownMenu.contains(event.target)) {

            this.setState({ showMenu: false }, () => {
                document.removeEventListener('click', this.closeMenu);
            });

        }
    }
    categoryPick(index) {
        if(index === "deleteFilters"){
            this.setState({
                productList: this.state.productsListOriginal
            })
        }
        else{
        let id = parseInt(index);
        let table = [];
        for (var i = 0; i < this.state.productsListOriginal.length; i++) {
            if (this.state.productsListOriginal[i].categoryId === id) {
                table.push(this.state.productsListOriginal[i])
            }
        }
        this.setState({ productList: table });
    }
    }

    renderCard = () => {
        let table = [];
        if (this.state.isLoading === false) {
            for (var i = 0; i < this.state.productList.length; i++) {
                let item = this.state.productList[i];

                if (item.imageLink == null) {
                    item.imageLink = image1
                }


                table.push(
                    <Card>
                        <Image style={{ height: '250px' }} src={item.imageLink} />
                        <Card.Content style={{ backgroundColor: '#e4f2f7' }}>
                            <Card.Header>{item.text}</Card.Header>
                            <Card.Meta>Nr id: {item.categoryId}</Card.Meta>
                            <Card.Description>Liczba sztuk w magazynie: {item.quantity}</Card.Description>
                        </Card.Content>
                        <Card.Content style={{ backgroundColor: '#96cbdf' }}>
                            <Label style={{ backgroundColor: 'yellow' }} as='a' tag>{item.price} PLN</Label>
                            <Button id={item.id} name={item.text} value={item.price} onClick={this.addToCart.bind(this)} animated='vertical' secondary style={{ left: 30 }}>
                                <Button.Content id={item.id} name={item.text} value={item.price} hidden>Zamów</Button.Content>
                                <Button.Content visible>
                                    <Icon name='shop' />
                                </Button.Content>
                            </Button>
                        </Card.Content>
                    </Card>
                )
            }
            console.log(table)
            return table;
        }
    }

    // eslint-disable-next-line no-dupe-class-members
    addToCart(e) {
        debugger
        if (e.target.name !== undefined) {
            this.props.triggerProductAddToCart(e.target.id, e.target.name, e.target.value);
        }
        else {
            this.props.triggerProductAddToCart(e.target.offsetParent.id, e.target.offsetParent.name, e.target.offsetParent.value);
        }
        toast({
            type: 'success',
            icon: 'check',
            title: 'Dodano produkt',
            description: 'Produkt został dodany do koszyka',
            time: 2000
        });
    }

    render() {
        return (
            <Segment style={{ minWidth: '350', minHeight: '350' }}>
                <Card.Group className="styledCard" centered items={this.product}>
                    {this.renderCard()}
                </Card.Group>
                <SemanticToastContainer position="bottom-right" />
            </Segment>
        )
    }
}