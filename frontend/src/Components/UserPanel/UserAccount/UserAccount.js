import React, { Component } from 'react'
import { Image, Modal, Button, Form, Input } from 'semantic-ui-react'
import axios from 'axios';
// import { toast } from 'react-semantic-toasts'
import store from './TokenStore'
// import qs from 'qs'
import avatarImage from '../../../img/malaPostac.png'


export default class UserAccountModal extends Component {

    state = {
        openUserAccount: false,
        userEmail: '',
        userFirstname: '',
        username: '',
        // emailConfirmed: false,
        userSecondname: ''
    }

    ShowUserAccount = () => {
        this.setState({
            openUserAccount: true
        })
        let token = store.data[0]
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/register/getuser`, {
                params: {
                    token: token
                }
            })
            .then(function (response) {
                console.log('token', response.data)
                self.setState({
                    userFirstname: response.data.firstName,
                    userSecondname: response.data.secondName,
                    username: response.data.userName,
                    // emailConfirmed: response.data.emailConfirmed,
                    userEmail: response.data.email

                })
                // setTimeout(() => {
                //     toast({
                //         type: 'success',
                //         icon: 'check',
                //         title: 'Dane zapisane poprawnie',
                //         description: 'Dane użytkownika ' + self.state.username + ' został zapisane poprawnie',
                //         time: 2000
                //     });
                // }, 20);
            })
        // console.log(this.state)
    }

    HiddenUserAccount = () => {
        this.setState({
            openUserAccount: false
        })
    }

    render() {
        return (
            <Modal style={{ paddingRight: '64px' }} size="tiny" onClose={this.HiddenUserAccount} open={this.state.openUserAccount} closeIcon>
                <Modal.Header centered>My account</Modal.Header>
                <Modal.Content >
                    <Modal.Content image>
                        <Image wrapped src={avatarImage} alt='avatar' avatar />
                        <Modal.Description>
                            <Form>
                                <Form.Field>
                                    <label>First name</label>

                                    <Input placeholder='First name' label={{ icon: 'asterisk', color: 'blue' }} labelPosition='right corner'
                                        value={this.state.userFirstname}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Second name</label>
                                    <Input label={{ icon: 'asterisk', color: 'blue' }} labelPosition='right corner' placeholder='Second name'
                                        value={this.state.userSecondname}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>User name</label>
                                    <Input placeholder='user name' label={{ icon: 'asterisk', color: 'blue' }} labelPosition='right corner'
                                        value={this.state.username}
                                    />
                                </Form.Field>

                                <Form.Field>
                                    <label>Email</label>
                                    <Input placeholder="email" label={{ icon: 'asterisk', color: 'blue' }} labelPosition='right corner'
                                        value={this.state.userEmail}
                                    />
                                </Form.Field>
                                {/* <Form.Field>
                                    <Checkbox label='I agree to the Terms and Conditions' />
                                </Form.Field> */}
                            </Form>
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>

                        <Button color='black' onClick={this.HiddenUserAccount} style={{ marginTop: '15px' }}>
                            Cancel
            </Button>
                        {/* <Button
                            positive
                            icon='checkmark'
                            labelPosition='right'
                            content="Save"
                            onClick={this.close}
                        /> */}

                    </Modal.Actions>
                </Modal.Content>
            </Modal>
        )
    }
}