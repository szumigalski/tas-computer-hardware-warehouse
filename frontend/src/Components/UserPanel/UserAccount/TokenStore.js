class Token {
    constructor() {
        this.data = [];
    }
    addToken(token) {
        this.data.push(token);
    }
}

const store = new Token();

Object.freeze(store)

export default store;