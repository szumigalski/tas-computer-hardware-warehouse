import React from 'react'
import backgroundImg from '../../../img/postac.png'

const FixedMenuLayout = () => (
    <div>
        <span>
            {/* <p style={{ fontWeight: '300', zIndex: 1, position: 'absolute', top: '5%', left: '5%', color: 'white', fontSize: '20px' }} name="warehouse" icon color='black'><Icon name='warehouse' size='large' />Computer Warehouse Hardware</p> */}
            <img src={backgroundImg} height='200%' width='100%' alt="logo" />
        </span>
    </div>
)

export default FixedMenuLayout
