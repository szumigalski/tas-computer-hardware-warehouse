import React, { Component } from 'react';
import MainMenu from '../MainMenu/MainMenu';
import RightMenu from '../RightMenu/RightMenu';
import Products from '../Products/Products';
import Footer from '../Footer/Footer';
import MenuTest from '../Menu/MenuTest';
import Basket from '../Basket/Basket'
import UserAccount from '../UserAccount/UserAccount'

export default class MainUser extends Component {

    constructor(props) {
        super(props);

        this.categoryChose = this.categoryChose.bind(this);
        this.openBasket = this.openBasket.bind(this);
        this.sendProductToCart = this.sendProductToCart.bind(this);
        this.openUserAccount = this.openUserAccount.bind(this);
        this.deleteFilters = this.deleteFilters.bind(this);
    }

    categoryChose(id) {
        this.refs.productComp.categoryPick(id);
    }

    openBasket() {
        this.refs.basket.ShowBasket();
    }
    openUserAccount() {
        this.refs.useraccount.ShowUserAccount();
    }

    sendProductToCart(index, name, price) {
        debugger
        this.refs.basket.addToCart(index, name, price);
    }

    deleteFilters(){
        this.refs.productComp.categoryPick("deleteFilters");
    }

    render() {
        return (
            <div>
                <div style={{ paddingRight: '64px' }}>
                    <MainMenu />
                    <MenuTest deleteFilters={this.deleteFilters} triggerCategoryRefresh={this.categoryChose} />
                    <Products ref="productComp" triggerProductAddToCart={this.sendProductToCart} />
                    <Footer />
                </div>
                <RightMenu onBasket={this.openBasket} onUserAccount={this.openUserAccount} />
                <Basket ref="basket" />
                <UserAccount ref="useraccount" />
            </div>
        )
    }
}