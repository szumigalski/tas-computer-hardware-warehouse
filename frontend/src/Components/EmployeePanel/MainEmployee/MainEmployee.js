import React, { Component } from 'react'
import { Segment, Table, Header, Icon } from 'semantic-ui-react'
import MainMenu from '../MainMenu/MainMenu'
import axios from 'axios';
import ModalEmployee from './ModalEmployee'
import _ from 'lodash'

export default class MainEmployee extends Component {

    constructor(props) {
        super(props);

        this.closeModal = this.closeModal.bind(this);
    }

    state={
        orderList: [],
        orderBasicList: [],
        openModal: false,
        order: '',
        statusList: [],
        productList: []
    }

    componentDidMount(){
        var self = this
        this.getStatus()
        this.getProduct()
        setTimeout( function() {
            self.getOrders()
        }, 200 )
    }

    openModal = (o) => {
        this.setState({
            openModal: !this.state.openModal,
            order: o
        })
        this.getOrderBasics()
    }

    closeModal(){
        debugger
        this.setState({
            openModal: false
        })
    }

    getOrders() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/order`)
            .then(function(response) {
                let orders =[]
                response.data.map(order=>{
                    return (
                        orders.push({
                            id: order.id,
                            startTime: order.startTime,
                            endTime: order.endTime,
                            status: order.status,
                            show: true
                        })
                )
                })
                console.log('lol', orders)
                self.setState({
                    orderList: orders
                })
            })
            .catch(function(error) {
                console.log(error);
			})
    }

    getStatus() {
        var self = this
            axios
                .get(`https://waremana.projektstudencki.pl/api/status`)
                .then(function(response) {
                    let statuses =[]
                    response.data.map(status=>{
                        return (
                            statuses.push({
                                id: status.id,
                                statusEnumNumber: status.statusEnumNumber,
                                statusName: status.statusName
                            })
                    )
                    })
                    self.setState({
                        statusList: statuses
                    })
                })
                .catch(function(error) {
                    console.log(error);
                })
    }

    getProduct() {
        var self = this
            axios
                .get(`https://waremana.projektstudencki.pl/api/product`)
                .then(function(response) {
                    let products =[]
                    response.data.map(product=>{
                        return (
                            products.push({
                                id: product.id,
                                name: product.name
                            })
                    )
                    })
                    self.setState({
                        productList: products
                    })
                })
                .catch(function(error) {
                    console.log(error);
                })
    }

    getOrderBasics() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/inorderbasic`)
            .then(function(response) {
                let orders =[]
                response.data.map(order=>{
                    return (
                        order.id === self.state.order.id && 
                        orders.push({
                            id: order.id,
                            status: _.find(self.state.statusList, { 'id': order.statusId} ).statusName,
                            product: _.find(self.state.productList, { 'id': order.productId} ).name,
                            whoDid: order.whoDid,
                        })
                )
                })
                console.log('order', orders)
                debugger
                self.refs.modal.willBeOpen(orders)
                self.setState({
                    orderBasicList: orders
                })
            })
            .catch(function(error) {
                console.log(error);
            })
    }

    timeConverter(time){
        var a = new Date(time);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
      }

    render(){
        return(
            <div>
                <MainMenu />
                <Segment style={{marginLeft: 10, overflowY: 'scroll', position: 'absolute', width: 'calc(100% - 20px)', height: 'calc(100% - 75px)' }}>
                    <Header as='h2' style={{position: 'sticky'}}>Orders</Header>
                    <Table celled inverted selectable>
                        <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell style={{textAlign: 'center'}}>Id</Table.HeaderCell>
                            <Table.HeaderCell style={{textAlign: 'center'}}>Start Date</Table.HeaderCell>
                            <Table.HeaderCell style={{textAlign: 'center'}}>End Date</Table.HeaderCell>
                            <Table.HeaderCell style={{textAlign: 'center'}}>Status</Table.HeaderCell>
                        </Table.Row>
                        </Table.Header>

                        <Table.Body>
                        {this.state.orderList.map( (order, i) => <Table.Row disabled={order.status} key={i} onClick={()=>this.openModal(order)}>
                                {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.id}</Table.Cell>}
                                {order.show && <Table.Cell style={{textAlign: 'center'}}>{this.timeConverter(order.startTime)}</Table.Cell>}
                                {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.endTime ? this.timeConverter(order.endTime) : 'In progress'}</Table.Cell>}
                                {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.status ? <Icon color="green" name="check circle" size='large'/> : <Icon color="red" name="remove circle" size='large' />}</Table.Cell>}
                            </Table.Row>)}
                        </Table.Body>
                    </Table>
                    <ModalEmployee
                    ref="modal"
                    closeModal={this.closeModal}
                    funcOpenModal={()=>this.openModal} 
                    openModal={this.state.openModal} 
                    orderBasicList={this.state.orderBasicList}
                    />
                </Segment>
            </div>
        )
    }
}