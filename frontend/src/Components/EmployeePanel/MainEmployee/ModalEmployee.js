import React, { Component } from 'react'
import { Card, Grid, Modal, Button, TransitionablePortal } from 'semantic-ui-react'
import axios from 'axios'
import { toast } from 'react-semantic-toasts'

export default class ModalEmployee extends Component {
    state={
        product: "",
        id: "",
        status: ""
    }

    willBeOpen(basicList){
        debugger
        this.setState({
            product: basicList[0].product,
            id: basicList[0].id,
            status: basicList[0].status
        })
    }

    toDoToInProgress = () =>{
        var self = this
        let url = 'https://waremana.projektstudencki.pl/api/inorderbasic?productid='+ this.state.id + '&statusId=2'

        axios({
                method: 'put',
                url: url
            })
            .then(function (response) {
                setTimeout(() => {
                toast({
                    type: 'success',
                    icon: 'check',
                    title: 'Edytowano status',
                    description: 'Status został edytowany poprawnie',
                    time: 2000
                });
            }, 20);
            self.state.refreshData()
            })
            .catch(function (error) {
                toast({
                type: 'error',
                icon: 'exclamation',
                title: 'Nie edytowano statusu',
                description: 'Nastąpił błąd, który uniemożliwił edycję statusu',
                time: 4000
            });
            });
        }

        changeProgress(){
            debugger
            if(this.state.status === 'Awaiting'){
                this.toDoToInProgress();
            }
        }

        close(){
            this.props.closeModal()
        }

        retTODO(){
            if(this.state.status === 'Awaiting'){
            return(
                <Card.Content 
                                        style={{
                                            display: 'flex', 
                                            alignItems: 'center',
                                            justifyContent: 'space-around'
                                            }}>
                                            {this.state.product}
                                            <Button 
                                            size='mini' 
                                            color='black' 
                                            circular 
                                            icon='caret right'
                                            onClick={this.changeProgress.bind(this)} 
                                            />
                                        </Card.Content>
            )
            }
        }

        retINPROG(){
            if(this.state.status === 'InProgress'){
            return(
                <Card.Content 
                                        style={{
                                            display: 'flex', 
                                            alignItems: 'center',
                                            justifyContent: 'space-around'
                                            }}>
                                            {this.state.product}
                                            <Button 
                                            size='mini' 
                                            color='black' 
                                            circular 
                                            icon='caret right'
                                            onClick={this.changeProgress.bind(this)} 
                                            />
                                        </Card.Content>
            )
                                        }
        }

        retDONE(){
            if(this.state.status === 'Done'){
            return(
                <Card.Content 
                                        style={{
                                            display: 'flex', 
                                            alignItems: 'center',
                                            justifyContent: 'space-around'
                                            }}>
                                            {this.state.product}
                                        </Card.Content>
            )
            }
        }

  render(){
      return(
        <TransitionablePortal
            open={this.props.openModal}
            onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
            transition={{ animation: 'scale', duration: 500 }}>
            <Modal open>
                    <Grid columns={3} style={{height: '100%', position: 'relative'}}>
                        <Grid.Row style={{height: '100%', marginTop: 15, padding: 30}}>
                            <Grid.Column>
                                <Card style={{width: '100%',height: '100%', border: '2px solid #ff9999'}}>
                                    <Card.Content header='To Do' style={{borderBottom: '2px solid red', backgroundColor: '#ff9999', textAlign: 'center'}}/>
                                    <Card.Content style={{height: '100%', backgroundColor: '#ffe6e6'}}>
                                        <Card style={{width: '100%'}}>
                                        {this.retTODO()}
                                    </Card>
                                    </Card.Content>
                                </Card>  
                            </Grid.Column>
                            <Grid.Column>
                            <Card style={{width: '100%',height: '100%', border: '2px solid #b3b3ff'}}>
                                <Card.Content header='In progress' style={{borderBottom: '2px solid blue', backgroundColor: '#b3b3ff', textAlign: 'center'}}/>
                                <Card.Content style={{height: '100%', backgroundColor: '#e6ecff'}}>
                                <Card style={{width: '100%'}}>
                                {this.retINPROG()}
                                </Card>
                                </Card.Content>
                            </Card>   
                            </Grid.Column>
                            <Grid.Column>
                                <Card style={{width: '100%',height: '100%', border: '2px solid #66ff66'}}>
                                    <Card.Content header='Done' style={{borderBottom: '2px solid green', backgroundColor: '#66ff66', textAlign: 'center'}}/>
                                    <Card.Content style={{height: '100%', backgroundColor: '#ccffcc'}}>
                                    <Card style={{width: '100%'}}>
                                    {this.retDONE()}
                                    </Card>
                                    </Card.Content>
                                </Card>   
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row style={{ padding: 30 }}>
                            <Button color='green' onClick={this.close.bind(this)}>Close</Button>
                        </Grid.Row>
                    </Grid>
            </Modal>
        </TransitionablePortal>
      )
  }
}