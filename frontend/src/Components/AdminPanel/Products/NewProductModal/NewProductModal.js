import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import _ from 'lodash'
import {  toast } from 'react-semantic-toasts'
export default class NewProductModal extends Component {
  state ={
    name: '',
    category: '',
    price: '',
    categoryList: [],
    tagList: [],
    parent: '',
    quantity: '',
    tag: '',
    tagId: '',
    imageLink: '',
    parentId: null
  }

  componentDidMount(){
    this.getCategories()
    this.getTags()
  }

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  handleChangeSelect = (e, { value }) => {
    let id = _.find(this.state.categoryList, {'value': value}).id
    this.setState({ parent: value, parentId: id })
  }

  handleChangeSelectTag = (e, { value }) => {
    let id = _.find(this.state.tagList, {'value': value}).id
    this.setState({ tag: value, tagId: id })
  }

  clearData() {
    this.setState({
      name: '',
      category: '',
      price: '',
      parent: '',
      quantity: '',
      tag: '',
      tagId: '',
      parentId: null,
      imageLink: ''
    })
  }
  
  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }
  getCategories() {
    var self = this
    axios
        .get(`https://waremana.projektstudencki.pl/api/category`)
        .then(function(response) {
            console.log(response.data)
            let categories =[]
            response.data.map(category=>{
                return categories.push({id: category.id, value: category.name, text: category.name, parent: category.parentId})
            })
            self.setState({
                categoryList: categories
            })
        })
        .catch(function(error) {
            console.log(error);
  })
}
  getTags() {
    var self = this
    axios
        .get(`https://waremana.projektstudencki.pl/api/tag`)
        .then(function(response) {
            console.log(response.data)
            let tags =[]
            response.data.map(tag=>{
                return tags.push({id: tag.id, value: tag.name, text: tag.name})
            })
            self.setState({
                tagList: tags
            })
        })
        .catch(function(error) {
            console.log(error);
  })
}

addProduct() {
    var self = this
    let name = this.state.name ? 'name='+this.state.name : ''
    let price = this.state.price ? '&price='+this.state.price : ''
    let quantity = this.state.quantity ? '&quantity=' + this.state.quantity : ''
    let categoryId = this.state.parentId ? '&categoryId=' + this.state.parentId : ''
    let tag = this.state.tagId ? '&tagId=' + this.state.tagId : ''
    let imageLink = this.state.imageLink ? '&imageLink=' + this.state.imageLink : ''
    let url = 'https://waremana.projektstudencki.pl/api/product?' + name + price + quantity + categoryId + tag + imageLink

  axios({
    method: 'post',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Dodano produkt',
          description: 'Produkt '+self.state.name+' został dodany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie dodano produktu',
      description: 'Nastąpił błąd, prawdopodobnie nie podano wsyzstkich wymaganych pól',
      time: 4000
  });
  });
}
  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Add new Product</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Name:</label>
                    <Input placeholder='Product name' value={this.state.name} onChange={this.handleChange('name')}/>
                </Form.Field>
                <Form.Field>    
                    <label>Quantity:</label>
                    <Input placeholder='Product quantity' type='number' value={this.state.quantity} onChange={this.handleChange('quantity')}/>
                </Form.Field>  
                <Form.Field>
                    <label>Price:</label>
                    <Input placeholder='Product price' type='number' value={this.state.price} onChange={this.handleChange('price')}/>
                </Form.Field>
                <Form.Field>
                    <label>Category:</label>
                    <Dropdown 
                        placeholder='Select parent Category' 
                        fluid search selection 
                        options={this.state.categoryList} 
                        value={this.state.parent}
                        onChange={this.handleChangeSelect}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Tag:</label>
                    <Dropdown 
                        placeholder='Select tag' 
                        fluid search selection 
                        options={this.state.tagList} 
                        value={this.state.tag}
                        onChange={this.handleChangeSelectTag}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Image link:</label>
                    <Input placeholder='Image link' value={this.state.imageLink} onChange={this.handleChange('imageLink')}/>
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.addProduct()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}

