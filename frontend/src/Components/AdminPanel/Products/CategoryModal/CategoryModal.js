import React, { Component } from 'react'
import { Grid, Form, Button, Modal, Input, TransitionablePortal, Dropdown } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { toast } from 'react-semantic-toasts'

export default class CategoryModal extends Component {
  updateCategory() {
    console.log('lololol', this.props.categoryParent)
    var self = this
    let name = this.props.categoryName ? 'name='+this.props.categoryName : ''
    let parent = this.props.categoryParentId ? '&parentId='+this.props.categoryParentId : ''
    let url = 'https://waremana.projektstudencki.pl/api/category/' + this.props.categoryId+ '?'  + name + parent

  axios({
    method: 'put',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Edytowano kategorię',
          description: 'Kategoria '+self.props.categoryName+' została zmieniona poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.props.refreshData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie edytowano kategorii',
      description: 'Nastąpił błąd, króty uniemożlwia edycję kategorii',
      time: 4000
  });
  });
}
  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 600}} >
        <Modal.Header>{this.props.categoryName}</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
        <Grid columns={2} padded='vertically'>
          <Grid.Column>
            <Form>
              <Modal.Description>
                <Form.Field style={{width: 300}}>
                  <label>Name:</label>
                  <Input 
                    placeholder='Category name' 
                    value={this.props.categoryName} 
                    onChange={this.props.handleChange('categoryName')}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Parent Category:</label>
                  <Dropdown 
                        placeholder='Select Parent Category' 
                        fluid search selection 
                        options={this.props.categoryList} 
                        value={this.props.categoryParent}
                        onChange={this.props.handleChangeCategory}
                        style={{width: 300}}
                    />
                </Form.Field>
              </Modal.Description>
            </Form>
          </Grid.Column>
        </Grid>
        <Button.Group>
            <Button color='red' onClick={this.props.closeModal}>Delete</Button>
            <Button onClick={this.props.closeModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>this.updateCategory()} positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}
CategoryModal.propTypes = {
    openModal: PropTypes.bool,
    closeModal: PropTypes.func,
    categoryId: PropTypes.number,
    categoryName: PropTypes.string,
    categoryParent: PropTypes.string,
    categoryParentId: PropTypes.string,
    categoryList: PropTypes.array,
    handleChange: PropTypes.func,
    refreshData: PropTypes.func
  };
