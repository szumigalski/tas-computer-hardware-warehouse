import React, { Component } from 'react'
import { Grid, Form, Button, Image, Modal, Input, TransitionablePortal, Dropdown } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { toast } from 'react-semantic-toasts'
import _ from 'lodash'
import noImage from '../../../../img/No_Image_Available.png'

export default class ProductModal extends Component {
  updateProduct() {
    var self = this
    let name = this.props.productName ? 'name='+this.props.productName : ''
    let price = this.props.productPrice ? '&price='+this.props.productPrice : ''
    let quantity = this.props.productQuantity ? '&quantity=' + this.props.productQuantity : ''
    let categoryId = this.props.productCategory ? '&categoryId=' + _.find(this.props.categoryList, {'value': this.props.productCategory}).id : ''
    let tag = this.props.productTag ? '&tagId=' + _.find(this.props.tagList, {'value': this.props.productTag}).id : ''
    let imageLink = this.props.productImageLink ? '&imageLink=' + this.props.productImageLink : ''
    let url = 'https://waremana.projektstudencki.pl/api/product/' + this.props.productId+ '?'  + name + price + quantity + categoryId + tag + imageLink

  axios({
    method: 'put',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Edytowano produkt',
          description: 'Produkt '+self.props.productName+' został zmieniony poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.props.refreshData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie edytowano produktu',
      description: 'Nastąpił błąd, króty uniemożlwia edycję produktu',
      time: 4000
  });
  });
}
  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 600}} >
        <Modal.Header>{this.props.productName}</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
        <Grid columns={2} padded='vertically'>
          <Grid.Column>
            <Image wrapped style={{width: 200}} src={this.props.productImageLink ? this.props.productImageLink : noImage} />
          </Grid.Column>
          <Grid.Column>
            <Form>
              <Modal.Description>
                <Form.Field>
                  <label>Name:</label>
                  <Input 
                    placeholder='Product name' 
                    value={this.props.productName} 
                    onChange={this.props.handleChange('productName')}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Category:</label>
                  <Dropdown 
                        placeholder='Select Category' 
                        fluid search selection 
                        options={this.props.categoryList} 
                        value={this.props.productCategory}
                        onChange={this.props.handleChangeSelect}
                    />
                </Form.Field>
                <Form.Field>
                  <label>Price:</label>
                  <Input 
                    placeholder='product price' 
                    label={{ basic: true, content: 'PLN' }}
                    labelPosition='right'
                    value={this.props.productPrice} 
                    onChange={this.props.handleChange('productPrice')}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Quantity:</label>
                  <Input 
                    placeholder='product quantity' 
                    value={this.props.productQuantity} 
                    onChange={this.props.handleChange('productQuantity')}
                  />
                </Form.Field>
                <Form.Field>
                    <label>Tag:</label>
                    <Dropdown 
                        placeholder='Select tag' 
                        fluid search selection 
                        options={this.props.tagList} 
                        value={this.props.productTag}
                        onChange={this.props.handleChangeSelectTag}
                    />
                </Form.Field>
                <Form.Field>
                  <label>Image link:</label>
                  <Input 
                    placeholder='product image link' 
                    value={this.props.productImageLink} 
                    onChange={this.props.handleChange('productImageLink')}
                  />
                </Form.Field>
              </Modal.Description>
            </Form>
          </Grid.Column>
        </Grid>
        <Button.Group>
            <Button onClick={this.props.closeModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>this.updateProduct()} positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}
ProductModal.propTypes = {
    openModal: PropTypes.bool,
    closeModal: PropTypes.func,
    imageLink: PropTypes.string,
    productId: PropTypes.string,
    productName: PropTypes.string,
    productCategory: PropTypes.string,
    productPrice: PropTypes.number,
    productQuantity: PropTypes.number,
    productTag: PropTypes.number,
    productList: PropTypes.array,
    tagList: PropTypes.array,
    handleChange: PropTypes.func,
    handleChangeSelectTag: PropTypes.func,
    handleChangeSelect: PropTypes.func,
    refreshData: PropTypes.func
  };
