import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import _ from 'lodash'
import {  toast } from 'react-semantic-toasts'
export default class NewCategoryModal extends Component {
  state ={
    name: '',
    category: '',
    price: '',
    categoryList: [],
    parent: '',
    parentId: null
  }

  componentDidMount(){
    this.getCategories()
  }

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  handleChangeSelect = (e, { value }) => {
    let id = _.find(this.state.categoryList, {'value': value}).id
    this.setState({ parent: value, parentId: id })
  }

  clearData() {
    this.setState({
      name: '',
      category: '',
      price: '',
      parent: '',
      parentId: null
    })
  }
  
  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }

  getCategories() {
    var self = this
    axios
        .get(`https://waremana.projektstudencki.pl/api/category`)
        .then(function(response) {
            console.log(response.data)
            let categories =[]
            response.data.map(category=>{
                return categories.push({id: category.id, value: category.name, text: category.name, parent: category.parentId})
            })
            self.setState({
                categoryList: categories
            })
        })
        .catch(function(error) {
            console.log(error);
  })
}

addCategory() {
    var self = this
    let parentId = this.state.parentId ? this.state.parentId : null
    let url
    if(parentId)
      url = 'https://waremana.projektstudencki.pl/api/category?name='+this.state.name+'&parentId='+parentId
    else
      url = 'https://waremana.projektstudencki.pl/api/category?name='+this.state.name
  axios({
    method: 'post',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Dodano kategorię',
          description: 'Kategoria '+self.state.name+' została dodana poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie dodano kategorii',
      description: 'Nastąpił błąd, prawdopodobnie już istnieje taka kategoria, lub nie podano nazwy',
      time: 4000
  });
  });
}
  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Add new Category</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Name:</label>
                    <Input placeholder='Category name' value={this.state.name} onChange={this.handleChange('name')}/>
                </Form.Field>
                <Form.Field>
                    <label>Category:</label>
                    <Dropdown 
                        placeholder='Select parent Category' 
                        fluid search selection 
                        options={this.state.categoryList} 
                        value={this.state.parent}
                        onChange={this.handleChangeSelect}
                    />
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.addCategory()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}

