import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import _ from 'lodash'
import {  toast } from 'react-semantic-toasts'
export default class NewTagModal extends Component {
  state = {
    name: '',
    screen: '',
    screenId: '',
    screensList: []
  }

  componentDidMount(){
    this.getScreens()
  }

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  handleChangeSelect = (e, { value }) => {
    let id = _.find(this.state.screensList, {'value': value}).id
    this.setState({ screen: value, screenId: id })
  }

  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }

  clearData() {
    this.setState({
      name: '',
      screen: ''
    })
  }

  getScreens() {
    var self = this
    axios
        .get(`https://waremana.projektstudencki.pl/api/workspace`)
        .then(function(response) {
            console.log(response.data)
            let screens =[]
            response.data.map(screen=>{
                return screens.push({id: screen.id, value: screen.name, text: screen.name})
            })
            self.setState({
                screensList: screens
            })
        })
        .catch(function(error) {
            console.log(error);
  })
}

addTag() {
  var self = this
  let name = this.state.name ? 'name='+ this.state.name : ''
  let screen = this.state.screenId ? '&screenId=' + this.state.screenId : null
  let url = 'https://waremana.projektstudencki.pl/api/tag?' + name + screen

  axios({
    method: 'post',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Dodano tag',
          description: 'Tag '+ self.state.name +' został dodany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie dodano taga',
      description: 'Nastąpił błąd, prawdopodobnie już istnieje taki tag, lub nie podano nazwy',
      time: 4000
  });
  });
}
  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Add new Tag</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Name:</label>
                    <Input placeholder='Tag name' value={this.state.name} onChange={this.handleChange('name')}/>
                </Form.Field>
                <Form.Field>
                    <label>Screen:</label>
                    <Dropdown 
                        placeholder='Select screen' 
                        fluid search selection 
                        options={this.state.screensList} 
                        value={this.state.screen}
                        onChange={this.handleChangeSelect}
                    />
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.addTag()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}

