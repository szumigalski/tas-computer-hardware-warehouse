import React, { Component } from 'react'
import { 
        Icon, 
        Segment, 
        Header, 
        Table, 
        Dropdown, 
        Button, 
        Input,
        Image 
    } from 'semantic-ui-react'
import ProductModal from './ProductModal/ProductModal';
import CategoryModal from './CategoryModal/CategoryModal'
import NewCategoryModal from './NewCategoryModal/NewCategoryModal';
import NewProductModal from './NewProductModal/NewProductModal';
import NewTagModal from './NewTagModal/NewTagModal';
import axios from 'axios';
import _ from 'lodash'
import { SemanticToastContainer } from 'react-semantic-toasts'
import { toast } from 'react-semantic-toasts'
import spinner from '../../../img/spinner.gif'

export default class Products extends Component {
    state={
        openModal: false,
        productName: '',
        productQuantity: '',
        productPrice: '',
        productCategory: '',
        productList: [],
        categoryList: [],
        tagList: [],
        openNewModalCategory: false,
        openNewModalProduct: false,
        openNewModalTag: false,
        openCategoryModal: false,
        productCategoryId: '',
        productTag: '',
        productTagId: '',
        productId: '',
        categoryParent: '',
        categoryId: '',
        categoryParentId: '',
        categoryName: '',
        nameFilter: '',
        quantityFilter: '',
        priceFilter: '',
        categoryFilter: '',
        tagFilter: '',
        spinner: false,
        productImageLink: ''
    }

    ModalOpenHandler(product) {
        this.setState({
            openModal: true,
            productId: product.id,
            productName: product.name,
            productQuantity: product.quantity,
            productPrice: product.price,
            productCategory: _.find(this.state.categoryList, {'id': product.categoryId}).value,
            productTag: this.getTagName(product.tag),
            productImageLink: product.imageLink
        })
    }

    NewModalOpenHandler(value) {
        if(value === 'category'){
            this.setState({
                openNewModalCategory: true
            })
        } else if(value === 'tag'){
            this.setState({
                openNewModalTag: true
            })
        } else if(value === 'categoryEdit'){
            this.setState({
                openCategoryModal: true
            })
        } else {
            this.setState({
                openNewModalProduct: true
            })
        }
    }

    closeModal = () =>{
        this.setState({
            openModal: false
        })
    }

    closeCategoryModal = () =>{
        this.setState({
            openCategoryModal: false
        })
    }

    closeNewModalCategory = () =>{
            this.setState({
                openNewModalCategory: false
            })
        this.getCategories()
    }

    closeNewModalProduct = () =>{
        this.setState({
            openNewModalProduct: false
        })
        this.getProducts()
}

    closeNewModalTag = () =>{
        this.setState({
            openNewModalTag: false
        })
}

    componentDidMount(){
        var self = this
        this.getCategories()
        this.getTags()
        this.setState({
            spinner: true
        })
        setTimeout( function() {
            self.getProducts()
        }, 300 )
    }

    getTags() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/tag`)
            .then(function(response) {
                console.log(response.data)
                let tags =[]
                response.data.map(tag=>{
                    return tags.push({id: tag.id, name: tag.name, value: tag.name,text: tag.name, screen: tag.screenId})
                })
                self.setState({
                    tagList: tags
                })
            })
            .catch(function(error) {
                console.log(error);
      })
    }

    getCategories() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/category`)
            .then(function(response) {
                console.log(response.data)
                let categories =[]
                response.data.map(category=>{
                    return (
                        categories.push({id: category.id, value: category.name, text: category.name, parent: category.parentId, childs: []}),
                        console.log(_.find(categories, {'id': category.parentId})),
                        category.parentId &&_.find(categories, {'id': category.parentId}).childs.push(category.id)
                )
                })
                self.setState({
                    categoryList: categories
                })
                console.log(self.state.categoryList)
            })
            .catch(function(error) {
                console.log(error);
			})
    }
    addCategory() {
        const category= {
            name: this.state.name,
            parentId: this.state.parentId
        }
        var config = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
            }
        axios
            .post(`https://waremana.projektstudencki.pl/api/category`, {category},
            config)
            .then(function(response) {
                console.log(response)
            })
            .catch(function(error) {
                console.log(error);
			})
    }

    getCategoryPath(product, list) {
        let path = product.name
        let id = product.categoryId
        path =  _.find(this.state.categoryList, {'id': id}).value
        let category = _.find(this.state.categoryList, {'id': id}) ? _.find(this.state.categoryList, {'id': id}).parent : null
        while(category) {
            id = _.find(list, {'id': id}).parent
            category = category = _.find(this.state.categoryList, {'id': id}) ? _.find(this.state.categoryList, {'id': id}).parent : null
            path = path + '/' +_.find(list, {'id': id}).value
        }
        return path
    }

    getTagName(tag) {
        if(tag && _.find(this.state.tagList, {'id': tag}))
        return _.find(this.state.tagList, {'id': tag}).name
    }

    refreshData=()=> {
        var self = this
        this.setState({
        productTag: '',
        productTagId: '',
        productId: '',
        categoryParent: '',
        categoryId: '',
        categoryParentId: '',
        categoryName: '',
        nameFilter: '',
        quantityFilter: '',
        priceFilter: '',
        categoryFilter: '',
        tagFilter: ''})
        this.getCategories()
        this.getTags()
        this.setState({
            spinner: true
        })
        setTimeout( function() {
            self.getProducts()
        }, 500 )
    }

    getProducts() {
        var self = this
        this.setState({
            spinner: true
        })
        axios
            .get(`https://waremana.projektstudencki.pl/api/product`)
            .then(function(response) {
                let products =[]
                response.data.map(product=>{
                    return products.push(
                        {
                            id: product.id,
                            name: product.name,
                            quantity: product.quantity,
                            price: product.price,
                            category: self.getCategoryPath(product, self.state.categoryList),
                            categoryId: product.categoryId,
                            tag: product.tagId,
                            tagName: self.getTagName(product.tagId),
                            imageLink: product.imageLink,
                            show: true
                        }
                    )
                })
                self.setState({
                    productList: products,
                    spinner: false
                })
            })
            .catch(function(error) {
                console.log(error);
			})
    }

    handleChange = name => event => {
        if(name)
        this.setState({
          [name]: event.target.value,
        })
      }

    handleFilterChange = name => event => {
        let self = this
        console.log('zmiana')
        this.setState({
          [name]: event.target.value,
        })
        setTimeout( function() {
            self.checkFilter()
        }, 100 )
      }
    
    handleChangeSelect = (e, { value }) => {
        let id = _.find(this.state.categoryList, {'value': value}).id
        this.setState({ productCategory: value, productCategoryId: id })
      }

      handleChangeCategory = (e, { value }) => {
        let id = _.find(this.state.categoryList, {'value': value}).id
        this.setState({ categoryParent: value, categoryParentId: id })
      }
    
      handleChangeSelectCategoryEdit = (e, { value }) => {
        let id = _.find(this.state.categoryList, {'value': value}).id
        let parentId = _.find(this.state.categoryList, {'value': value}) ? _.find(this.state.categoryList, {'value': value}).parent : null
        let parent = parentId ? _.find(this.state.categoryList, {'id': parentId}).value : null
        this.setState({ categoryParent: parent, categoryId: id, categoryName: value })
      }

      handleChangeSelectTag = (e, { value }) => {
        let id = _.find(this.state.tagList, {'value': value}).id
        this.setState({ productTag: value, productTagId: id })
      }

      checkFilter(){
          let list = this.state.productList
          this.state.productList.map(product=>{
              if( (this.state.nameFilter && (product.name.search(this.state.nameFilter) === -1)) || 
                (this.state.priceFilter && (product.price.toString().search(this.state.priceFilter ) === -1)) || 
                (this.state.quantityFilter && (product.quantity.toString().search(this.state.quantityFilter ) === -1)) ||
                (this.state.tagFilter && (product.tagName.search(this.state.tagFilter) === -1)) ||
                (this.state.categoryFilter && (product.category.search(this.state.categoryFilter) === -1))
                ) {
                  _.find(list, {'id': product.id}).show = false
              }
              else {
                _.find(list, {'id': product.id}).show = true
              }
          })
          this.setState({
            productList: list
          })
      }

    deleteProduct (id, name) {
        var self = this
        axios
            .delete(`https://waremana.projektstudencki.pl/api/product/${id}`)
            .then(function(response) {
                console.log(response.data)
                setTimeout(() => {
                    toast({
                        type: 'success',
                        icon: 'check',
                        title: 'Usunięto produkt',
                        description: 'Produkt '+ name +' został usunięty poprawnie',
                        time: 2000
                    });
                    self.refreshData()
                }, 20);
            })
            .catch(function(error) {
                console.log(error);
                toast({
                    type: 'error',
                    icon: 'exclamation',
                    title: 'Produkt nie został usunięty',
                    description: 'Nastąpił błąd, produkt nie istnieje, bądź występuje problem z połączeniem z bazą danych',
                    time: 4000
                });
      })
    }

    deleteCategory (id, name) {
        var self = this
        axios
            .delete(`https://waremana.projektstudencki.pl/api/category/${id}`)
            .then(function(response) {
                console.log(response.data)
                setTimeout(() => {
                    toast({
                        type: 'success',
                        icon: 'check',
                        title: 'Usunięto kategorię',
                        description: 'Kategoria '+ name +' została usunięta poprawnie',
                        time: 2000
                    });
                    self.refreshData()
                }, 20);
            })
            .catch(function(error) {
                console.log(error);
                toast({
                    type: 'error',
                    icon: 'exclamation',
                    title: 'Kategoria nie został usunięta',
                    description: 'Nastąpił błąd, kategoria nie istnieje, bądź występuje problem z połączeniem z bazą danych',
                    time: 4000
                });
      })
    }
    deleteCategoryPath(category) {
        if(!category.childs) {
            this.state.productList.map(product=> {
                if(category.id === product.categoryId)
                 this.deleteProduct(product.id, product.name)
            })
            return this.deleteCategory(category.id, category.name)
        } else
            category.childs.map(childCategory=>{
                return this.deleteCategoryPath(_.find(this.state.categoryList, {'id': childCategory}))
            })
            this.state.productList.map(product=> {
                if(category.id === product.categoryId)
                    return this.deleteProduct(product.id, product.name)
            })
            return this.deleteCategory(category.id, category.name)
    }
  render(){
      return(
        <Segment 
        style={{
            overflowY: 'scroll', 
            position: 'absolute', 
            left: this.props.isMenu ? '270px': '70px', 
            width: this.props.isMenu ? 'calc(100% - 280px)' : 'calc(100% - 70px)', 
            height: 'calc(100% - 75px)' }} 
            scroll
            >
              <Header as='h2' style={{position: 'sticky'}}>Products</Header>
              <div style={{
                  display: 'flex', 
                  flexDirection: 'row', 
                  justifyContent: 'space-around', 
                  border: '10px solid rgb(100,100,100)',
                  backgroundColor: ' rgb(100,100,100)',
                  borderRadius: 5
                  }}>
                <Button style={{height: 40}} onClick={()=>this.NewModalOpenHandler('product')} secondary>Add Product</Button>
                <Button style={{height: 40}} onClick={()=>this.NewModalOpenHandler('tag')} secondary>Add Tag</Button>
              </div>
              <div 
                style={{
                    display: 'flex', 
                    flexDirection: 'row', 
                    justifyContent: 'space-around',
                    border: '10px solid rgb(50,50,50)',
                    backgroundColor: ' rgb(50,50,50)',
                    borderRadius: 5,
                    marginTop: 10
                    }}>
                <Button style={{height: 40}} onClick={()=>this.NewModalOpenHandler('category')} secondary>Add Category</Button>
                <div style={{display: 'flex', minWidth: 600}}>
                    <Dropdown 
                        placeholder='Select category' 
                        fluid selection 
                        options={this.state.categoryList} 
                        style={{width: '50%'}}
                        onChange={this.handleChangeSelectCategoryEdit}
                    />
                    <Button 
                        disabled={this.state.categoryName === ''}
                        style={{height: 40}} 
                        onClick={()=>this.NewModalOpenHandler('categoryEdit')} 
                        color='blue'
                        >
                        Edit Category
                    </Button>
                    <Button 
                        disabled={this.state.categoryName === ''}
                        style={{height: 40}} 
                        onClick={()=>this.deleteCategoryPath('categoryEdit')} 
                        color='red'
                        >
                        Delete Category
                    </Button>
                </div>
              </div>
              <Table celled inverted selectable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Name</Table.HeaderCell>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Quantity</Table.HeaderCell>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Price</Table.HeaderCell>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Category</Table.HeaderCell>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Tag</Table.HeaderCell>
                        <Table.HeaderCell style={{textAlign: 'center'}}>Delete</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    <Table.Row key={-1}>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 120, margin: 5}}  
                              placeholder='Name filter' 
                              value={this.state.nameFilter} 
                              onChange={this.handleFilterChange('nameFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 120, margin: 5}} 
                              placeholder='Quantity filter' 
                              value={this.state.quantityFilter} 
                              onChange={this.handleFilterChange('quantityFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 120, margin: 5}} 
                              placeholder='Price filter' 
                              value={this.state.priceFilter} 
                              onChange={this.handleFilterChange('priceFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 250, margin: 5}}
                              placeholder='Category filter' 
                              value={this.state.categoryFilter} 
                              onChange={this.handleFilterChange('categoryFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 120, margin: 5}}
                              placeholder='Tag filter' 
                              value={this.state.tagFilter} 
                              onChange={this.handleFilterChange('tagFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}/>
                    </Table.Row>
                    {this.state.spinner &&<Image src={spinner} centered style={{ height: '240px'}}/>}
                    {this.state.productList.map( (product, i) => <Table.Row key={i}>
                        {product.show && <Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(product)}>{product.name}</Table.Cell>}
                        {product.show && <Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(product)}>{product.quantity}</Table.Cell>}
                        {product.show && <Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(product)}>{product.price.toFixed(2)}<label style={{float: 'right'}} >PLN</label></Table.Cell>}
                        {product.show && <Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(product)}>{product.category}</Table.Cell>}
                        {product.show && <Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(product)}>{this.getTagName(product.tag)}</Table.Cell>}
                        {product.show && <Table.Cell style={{width: 50}}><Button icon color='red' onClick={()=>this.deleteProduct(product.id, product.name)}><Icon name='trash'/></Button></Table.Cell>}
                    </Table.Row>)}
                </Table.Body>

            </Table>
            <ProductModal 
                handleChangeSelect={this.handleChangeSelect}
                closeModal={this.closeModal}
                openModal={this.state.openModal}
                productId={this.state.productId}
                productName={this.state.productName}
                productQuantity={this.state.productQuantity}
                productPrice={this.state.productPrice}
                productCategory={this.state.productCategory}
                productImageLink={this.state.productImageLink}
                productTag={this.state.productTag}
                categoryList={this.state.categoryList}
                tagList={this.state.tagList}
                handleChange={this.handleChange}
                handleChangeSelectTag={this.handleChangeSelectTag}
                refreshData={this.refreshData}
            />
            <CategoryModal 
                handleChangeCategory={this.handleChangeCategory}
                closeModal={this.closeCategoryModal}
                openModal={this.state.openCategoryModal}
                categoryId={this.state.categoryId}
                categoryName={this.state.categoryName}
                categoryParent={this.state.categoryParent}
                categoryParentId={this.state.categoryParentId}
                categoryList={this.state.categoryList}
                handleChange={this.handleChange}
                refreshData={this.refreshData}
            />
            <NewCategoryModal 
                closeModal={this.closeNewModalCategory}
                openModal={this.state.openNewModalCategory}
                getCategories={this.getCategories}
            />
            <NewProductModal 
                closeModal={this.closeNewModalProduct}
                openModal={this.state.openNewModalProduct}
                getProducts={this.getProducts}
            />
            <NewTagModal 
                closeModal={this.closeNewModalTag}
                openModal={this.state.openNewModalTag}
            />
          <SemanticToastContainer position="bottom-right"/>
          </Segment>
      )
  }
}