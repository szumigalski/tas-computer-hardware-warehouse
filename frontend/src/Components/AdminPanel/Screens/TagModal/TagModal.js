import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import {  toast } from 'react-semantic-toasts'
import PropTypes from 'prop-types'

export default class TagModal extends Component {

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }

  clearData() {
    this.setState({
      name: '',
      screen: ''
    })
  }

updateTag() {
  var self = this
  let id = this.props.tagId
  let name = this.props.tagName ? 'name='+ this.props.tagName : ''
  let screen = this.props.tagScreenId ? '&screenId=' + this.props.tagScreenId : null
  let url = 'https://waremana.projektstudencki.pl/api/tag/'+ id +'?' + name + screen

  axios({
    method: 'put',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Edytowano tag',
          description: 'Tag '+ self.state.name +' został edytowany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  self.props.refreshData()
  })
  .catch(function (error) {
    console.log('no')
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie edytowano tagu',
      description: 'Nastąpił błąd, który uniemożliwił edycję tagu',
      time: 4000
  });
  });
}

  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Edit tag</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Name:</label>
                    <Input placeholder='Tag name' value={this.props.tagName} onChange={this.props.handleChange('tagName')}/>
                </Form.Field>
                <Form.Field>
                    <label>Screen:</label>
                    <Dropdown 
                        placeholder='Select screen' 
                        fluid search selection 
                        options={this.props.screensList} 
                        value={this.props.tagScreen}
                        onChange={this.props.handleChangeSelect}
                    />
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.updateTag()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}

TagModal.propTypes = {
  openModal: PropTypes.bool,
  closeModal: PropTypes.func,
  tagName: PropTypes.string,
  tagScreen: PropTypes.string,
  tagScreenId: PropTypes.string,
  handleChange: PropTypes.func,
  screensList: PropTypes.array,
  tagId: PropTypes.any,
  refreshData: PropTypes.func
};