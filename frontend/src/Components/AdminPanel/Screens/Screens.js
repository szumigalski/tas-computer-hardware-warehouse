import React, { Component } from 'react'
import { 
    Segment, 
    Header, 
    Table, 
    Button, 
    Grid, 
    Icon,
    Input,
    Image 
} from 'semantic-ui-react'
import axios from 'axios';
import _ from 'lodash'
import {  toast } from 'react-semantic-toasts'
import TagModal from './TagModal/TagModal'
import spinner from '../../../img/spinner.gif'

export default class Screens extends Component {

    state={
        tagId: '',
        tagList: [],
        screensList: [],
        openModalTag: false,
        tagName: '',
        tagScreen: '',
        tagScreenId: '',
        screenNameInput: '',
        tagNameFilter: '',
        tagScreenFilter: '',
        screenNameFilter: '',
        spinner: false
      }

    componentDidMount(){
        var self = this
        this.getScreens()
        this.setState({
            spinner: true
        })
        setTimeout( function() {
            self.getTags()
        }, 300 )
      }
      
    refreshData=()=> {
        var self = this
        this.getScreens()
        this.setState({
            spinner: true
        })
        setTimeout( function() {
            self.getTags()
        }, 300 )
    }
    ModalOpenHandler(tag) {
        this.setState({
            openModalTag: true,
            tagName: tag.name,
            tagScreen: this.getScreenName(tag.screen),
            tagScreenId: tag.screen,
            tagId: tag.id
        })
    }

    getTags() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/tag`)
            .then(function(response) {
                console.log(response.data)
                let tags =[]
                response.data.map(tag=>{
                    return tags.push({
                        id: tag.id, 
                        name: tag.name, 
                        screen: tag.screenId,
                        screenName: self.getScreenName(tag.screenId),
                        show: true
                    })
                })
                self.setState({
                    tagList: tags,
                    spinner: false
                })
            })
            .catch(function(error) {
                console.log(error);
      })
    }

    handleChange = name => event => {
        if(name)
        this.setState({
          [name]: event.target.value,
        })
      }

    getScreens() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/workspace`)
            .then(function(response) {
                console.log(response.data)
                let screens =[]
                response.data.map(screen=>{
                    return screens.push({
                        id: screen.id, 
                        name: screen.name,
                        value: screen.name, 
                        text: screen.name,
                        show: true 
                    })
                })
                self.setState({
                    screensList: screens
                })
            })
            .catch(function(error) {
                console.log(error);
      })
    }

    handleChangeSelect = (e, { value }) => {
        let id = _.find(this.state.screensList, {'value': value}).id
        this.setState({ tagScreen: value, tagScreenId: id })
      }

    deleteTag (id, name) {
        var self = this
        axios
            .delete(`https://waremana.projektstudencki.pl/api/tag/${id}`)
            .then(function(response) {
                console.log(response.data)
                setTimeout(() => {
                    toast({
                        type: 'success',
                        icon: 'check',
                        title: 'Usunięto tag',
                        description: 'Tag '+ name +' został usunięty poprawnie',
                        time: 2000
                    });
                    self.getTags()
                }, 20);
            })
            .catch(function(error) {
                console.log(error);
                toast({
                    type: 'error',
                    icon: 'exclamation',
                    title: 'Tag nie został usunięty',
                    description: 'Nastąpił błąd, tag nie istnieje, bądź występuje problem z połączeniem z bazą danych',
                    time: 4000
                });
      })
    }

    getScreenName(screenId){
        console.log('screen', screenId)
        if(screenId)
        return _.find(this.state.screensList, {'id': screenId}).value
    }

    closeModalTag = () =>{
        this.setState({
            openModalTag: false
        })
}

addScreen() {
    var self = this
    let name = this.state.screenNameInput ? 'name='+this.state.screenNameInput : ''
    let url = 'https://waremana.projektstudencki.pl/api/workspace?' + name
  
  axios({
    method: 'post',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Dodano ekran',
          description: 'Ekran '+self.state.screenNameInput+' został dodany poprawnie',
          time: 2000
      });
  }, 20);
  self.refreshData()
  self.setState({
    screenNameInput: ''
  })
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie dodano ekranu',
      description: 'Nastąpił błąd, prawdopodobnie nie podano wsyzstkich wymaganych pól',
      time: 4000
  });
  });
  }

  deleteScreen (id, name) {
    var self = this
    axios
        .delete(`https://waremana.projektstudencki.pl/api/workspace/${id}`)
        .then(function(response) {
            console.log(response.data)
            setTimeout(() => {
                toast({
                    type: 'success',
                    icon: 'check',
                    title: 'Usunięto ekran',
                    description: 'Ekran '+ name +' został usunięty poprawnie',
                    time: 2000
                });
                self.refreshData()
            }, 20);
        })
        .catch(function(error) {
            console.log(error);
            toast({
                type: 'error',
                icon: 'exclamation',
                title: 'Ekran nie został usunięty',
                description: 'Nastąpił błąd, ekran nie istnieje, bądź występuje problem z połączeniem z bazą danych',
                time: 4000
            });
  })
}
handleFilterChangeTag = name => event => {
    let self = this
    console.log('zmiana')
    this.setState({
      [name]: event.target.value,
    })
    setTimeout( function() {
        self.checkFilterTag()
    }, 100 )
  }

  handleFilterChangeScreen = name => event => {
    let self = this
    console.log('zmiana')
    this.setState({
      [name]: event.target.value,
    })
    setTimeout( function() {
        self.checkFilterScreen()
    }, 100 )
  }

  checkFilterTag(){
    let list = this.state.tagList
    this.state.tagList.map(tag=>{
        if( (this.state.tagNameFilter && (tag.name.search(this.state.tagNameFilter) === -1)) || 
          (this.state.tagScreenFilter && (tag.screenName.search(this.state.tagScreenFilter ) === -1))
          ) {
            _.find(list, {'id': tag.id}).show = false
        }
        else {
          _.find(list, {'id': tag.id}).show = true
        }
    })
    this.setState({
      tagList: list
    })
}

checkFilterScreen(){
    let list = this.state.screensList
    this.state.screensList.map(screen=>{
        if( (this.state.screenNameFilter && (screen.name.search(this.state.screenNameFilter) === -1))
          ) {
            _.find(list, {'id': screen.id}).show = false
        }
        else {
          _.find(list, {'id': screen.id}).show = true
        }
    })
    this.setState({
      screensList: list
    })
}

  render(){
      return(
        <Segment 
            style={{
                overflowY: 'scroll', 
                position: 'absolute', 
                left: this.props.isMenu ? '270px': '70px', 
                width: this.props.isMenu ? 'calc(100% - 280px)' : 'calc(100% - 70px)', 
                height: 'calc(100% - 75px)' }} 
                scroll
                >
        <Header as='h2' style={{position: 'sticky'}}>Screens</Header>
        <Grid>
            <Grid.Row columns={2}>
                <Grid.Column>
                    <Header as='h3' style={{textAlign: 'center'}}>Tags</Header>
                    <Table celled inverted selectable>
                        <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Screen</Table.HeaderCell>
                            <Table.HeaderCell>Delete</Table.HeaderCell>
                        </Table.Row>
                        </Table.Header>

                        <Table.Body>
                        <Table.Row key={-1+'u'}>
                                <Table.Cell style={{textAlign: 'center', padding: 0}}/>
                                <Table.Cell style={{textAlign: 'center', padding: 0}}>
                                    <Input 
                                    style={{maxWidth: 120, margin: 5}}  
                                    placeholder='Name filter' 
                                    value={this.state.tagNameFilter} 
                                    onChange={this.handleFilterChangeTag('tagNameFilter')}
                                    />
                                </Table.Cell>
                                <Table.Cell style={{textAlign: 'center', padding: 0}}>
                                    <Input 
                                    style={{maxWidth: 120, margin: 5}} 
                                    placeholder='Screen filter' 
                                    value={this.state.tagScreenFilter} 
                                    onChange={this.handleFilterChangeTag('tagScreenFilter')}
                                    />
                                </Table.Cell>
                                <Table.Cell style={{textAlign: 'center', padding: 0}}/>
                            </Table.Row>
                            {this.state.spinner &&<Image src={spinner} centered style={{ height: '240px'}}/>}
                            {this.state.tagList.map( (tag, key) => <Table.Row key={key}>
                                {tag.show &&<Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(tag)} style={{width: 20}}>{tag.id}</Table.Cell>}
                                {tag.show &&<Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(tag)} >{tag.name}</Table.Cell>}
                                {tag.show &&<Table.Cell style={{textAlign: 'center'}} onClick={()=>this.ModalOpenHandler(tag)}>{this.getScreenName(tag.screen)}</Table.Cell>}
                                {tag.show &&<Table.Cell style={{width: 50}}><Button icon color='red' onClick={()=>this.deleteTag(tag.id, tag.name)}><Icon name='trash'/></Button></Table.Cell>}
                            </Table.Row>)}
                        </Table.Body>
                    </Table>
                </Grid.Column>  
                <Grid.Column style={{textAlign: 'center'}}>
                    <Header as='h3' style={{textAlign: 'center'}}>Screens</Header>
                    <Table celled inverted selectable>
                        <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Delete</Table.HeaderCell>
                        </Table.Row>
                        </Table.Header>

                        <Table.Body>
                        <Table.Row key={-1}>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}/>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}>
                            <Input 
                              style={{maxWidth: 120, margin: 5}}  
                              placeholder='Name filter' 
                              value={this.state.screenNameFilter} 
                              onChange={this.handleFilterChangeScreen('screenNameFilter')}
                            />
                        </Table.Cell>
                        <Table.Cell style={{textAlign: 'center', padding: 0}}/>
                    </Table.Row>
                            {this.state.screensList.map( (screen,key) => <Table.Row key={key}>
                                {screen.show &&<Table.Cell style={{textAlign: 'center'}} style={{width: 20}}>{screen.id}</Table.Cell>}
                                {screen.show &&<Table.Cell style={{textAlign: 'center'}} >{screen.value}</Table.Cell>}
                                {screen.show &&<Table.Cell style={{width: 50}}><Button icon color='red' onClick={()=>this.deleteScreen(screen.id, screen.value)}><Icon name='trash'/></Button></Table.Cell>}
                            </Table.Row>)}
                            <Table.Row>
                                <Table.Cell style={{width: 20}}>{this.state.screensList.length+1}</Table.Cell>
                                <Table.Cell style={{textAlign: 'center'}}>
                                    <Input 
                                        placeholder='Screen name' 
                                        value={this.state.screenNameInput} 
                                        onChange={this.handleChange('screenNameInput')}
                                    />
                                </Table.Cell>
                                <Table.Cell style={{width: 50}}>
                                    <Button 
                                        icon 
                                        color='green' 
                                        onClick={()=>this.addScreen()}
                                        disabled={this.state.screenNameInput === ''}
                                    >
                                        <Icon name='plus'/>
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </Grid.Column>  
            </Grid.Row>
        </Grid>
        <TagModal 
            tagName={this.state.tagName}
            tagScreen={this.state.tagScreen}
            tagScreenId={this.state.tagScreenId}
            closeModal={this.closeModalTag}
            openModal={this.state.openModalTag}
            handleChange={this.handleChange}
            handleChangeSelect={this.handleChangeSelect}
            screensList={this.state.screensList}
            tagId={this.state.tagId}
            refreshData={this.refreshData}
        />
    </Segment>
      )
  }
}