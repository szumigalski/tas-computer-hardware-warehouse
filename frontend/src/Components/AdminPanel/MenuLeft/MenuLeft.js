import React, { Component } from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import { withRouter } from "react-router-dom"

class MenuLeft extends Component {
  state = { 
    activeItem: '/admin/charts'
   }

  
  handleItemClick(name) { 
    this.setState({ activeItem: '/admin/' + name })
    this.setHistory(name)
  }
  componentDidMount() {
    if(this.props.history.location.pathname !== '/admin')
      this.setState({ activeItem: this.props.history.location.pathname })
  }

  setHistory(name) {
    if(name === 'employees') {
      this.props.history.push("/admin/employees")
    } else
    if(name === 'products') {
      this.props.history.push("/admin/products")
    } else
    if(name === 'orders') {
      this.props.history.push("/admin/orders")
    } else
      if(name === 'screens') {
        this.props.history.push("/admin/screens")
    } else
      if(name === 'charts') {
        this.props.history.push("/admin/charts")
  }

  }
  render() {
    const { activeItem } = this.state

    return (
      <Menu pointing vertical color='grey' inverted size='large' borderless style={{ height: 'calc(100% - 53px)', position: 'absolute', width: this.props.isMenu ? '18rem': '4rem', transition: 'width 0.1s'}}>
        <Menu.Item
          active={activeItem === '/admin/charts'} 
          style={{fontSize: 18, display: 'flex'}}
          onClick={()=>this.handleItemClick('charts')}><Icon name='pie chart' style={{float: 'left', margin: 0, marginRight: 15, minHeight: 20}}/>{this.props.isMenu ? 'Charts': <span style={{opacity: 0}}>'.'</span>}</Menu.Item>
        <Menu.Item
          active={activeItem === '/admin/products'} 
          style={{fontSize: 18, display: 'flex'}}
          onClick={()=>this.handleItemClick('products')}><Icon name='box' style={{float: 'left', margin: 0, marginRight: 15}}/>{this.props.isMenu ? 'Products': <span style={{opacity: 0}}>'.'</span>}</Menu.Item>
        <Menu.Item
          active={activeItem === '/admin/employees'}
          style={{fontSize: 18, display: 'flex'}}
          onClick={()=>this.handleItemClick('employees')}
        ><Icon name='users' style={{float: 'left', margin: 0, marginRight: 15}}/>{this.props.isMenu ? 'Employees': <span style={{opacity: 0}}>'.'</span>}</Menu.Item>
        <Menu.Item
          active={activeItem === '/admin/orders'}
          style={{fontSize: 18, display: 'flex'}}
          onClick={()=>this.handleItemClick('orders')}
        ><Icon name='cart' style={{float: 'left', margin: 0, marginRight: 15}}/>{this.props.isMenu ? 'Orders': <span style={{opacity: 0}}>'.'</span>}</Menu.Item>
        <Menu.Item
          active={activeItem === '/admin/screens'}
          style={{fontSize: 18, display: 'flex'}}
          onClick={()=>this.handleItemClick('screens')}
        ><Icon name='columns' style={{float: 'left', margin: 0, marginRight: 15}}/>{this.props.isMenu ? 'Screens': <span style={{opacity: 0}}>'.'</span>}</Menu.Item>
        <Menu.Item
          active={activeItem === '/admin/screens'}
          style={{
            fontSize: 18, 
            position: 'inherit', 
            bottom: 0, 
            width: '100%',
            borderTop: '1px solid white'
          }}
          onClick={()=>this.props.menuShow()}
        >{this.props.isMenu ? <Icon 
          name='caret square left' 
          style={{
            float: 'right',
            left: 200}}/> : 
            <Icon 
              name='caret square right' 
              style={{
              float: 'right',
              left: 200}}/>
            }
          </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(MenuLeft)