import React, { Component } from 'react'
import MainMenu from '../MainMenu/MainMenu';
import MenuLeft from '../MenuLeft/MenuLeft';
import Charts from '../Charts/Charts';

export default class MainAdmin extends Component {

  menuShow = () =>{
    this.props.menuShow()
  }

  render(){
      return(
          <div>
            <MainMenu />
            <MenuLeft 
              isMenu={this.props.isMenu}
              menuShow={()=>this.menuShow()}
              />
            <Charts
              isMenu={this.props.isMenu}
              />
          </div>
      )
  }
}