import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import { toast } from 'react-semantic-toasts'

export default class EmployeeModal extends Component {

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }

  clearData() {
    this.setState({
      name: '',
      screen: ''
    })
  }

updateEmployee() {
  var self = this
  let username = this.props.employee.username 
  let firstname = this.props.employee.firstname ? this.props.employee.firstname : ''
  let secondname = this.props.employee.secondname ? this.props.employee.secondname : ''
  let email = this.props.employee.email ? this.props.employee.email : ''
  let phoneNumber = this.props.employee.phoneNumber ? this.props.employee.phoneNumber : ''
  let url = 'https://waremana.projektstudencki.pl/api/emloyee/'+ username +'?' + firstname + secondname

  axios({
    method: 'put',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Edytowano pracownika',
          description: 'Pracownik '+ self.state.name +' został edytowany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  self.props.refreshData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie edytowano pracownika',
      description: 'Nastąpił błąd, który uniemożliwił edycję pracownika',
      time: 4000
  });
  });
}

  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Edit employee</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Username:</label>
                    <Input 
                      placeholder='Employee usernamename' 
                      value={this.props.employee ? this.props.employee.username : null} 
                      onChange={()=>this.props.handleChange('username')}/>
                </Form.Field>
                <Form.Field>
                    <label>Firstname:</label>
                    <Input 
                      placeholder='Employee firstname' 
                      value={this.props.employee ? this.props.employee.firstname : '' } 
                      onChange={()=>this.props.handleChange('firstname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Secondname:</label>
                    <Input 
                      placeholder='Employee secondname' 
                      value={this.props.employee ? this.props.employee.secondname : ''} 
                      onChange={()=>this.props.handleChange('secondname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Email:</label>
                    <Input 
                      placeholder='Employee email' 
                      value={this.props.employee ? this.props.employee.email : ''} 
                      onChange={()=>this.props.handleChange('email')}/>
                </Form.Field>
                <Form.Field>
                    <label>Phone number:</label>
                    <Input 
                      placeholder='Employee phone number' 
                      value={this.props.employee ? this.props.employee.phoneNumber : ''} 
                      onChange={()=>this.props.handleChange('phoneNumber')}/>
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.updateEmployee()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}