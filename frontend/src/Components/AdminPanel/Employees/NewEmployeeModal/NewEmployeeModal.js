import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import { toast } from 'react-semantic-toasts'


export default class NewEmployeeModal extends Component {

  state={
    username: '',
    firstname: '',
    secondname: '',
    email: '',
    phoneNumber: '',
    password: ''
  }

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  closeThisModal = () => {
    this.clearData()
    this.props.closeNewModal()
  }

  clearData() {
    this.setState({
      username: '',
      firstname: '',
      secondname: '',
      email: '',
      phoneNumber: '',
      password: ''
    })
  }

addEmployee() {
  var self = this
  let username = this.state.username 
  let firstname = this.state.firstname ? '&firstname='+ this.state.firstname : ''
  let secondname = this.state.secondname ? '&secondname='+ this.state.secondname : ''
  let password = this.state.password ? '&password='+ this.state.password : ''
  let email = this.state.email ? '&email='+ this.state.email : ''
  let phoneNumber = this.state.phoneNumber ? '&phoneNumber='+ this.state.phoneNumber : ''
  let url = 'https://waremana.projektstudencki.pl/api/register/employee?username='+ username + firstname + secondname + email + phoneNumber + password

  axios({
    method: 'post',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Dodano pracownika',
          description: 'Pracownik '+ self.state.username +' został dodany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeNewModal()
  self.clearData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie dodano pracownika',
      description: 'Nastąpił błąd, który uniemożliwił dodanie pracownika',
      time: 4000
  });
  });
}

  render(){
      return(
        <TransitionablePortal
          open={this.props.openNewModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Edit employee</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Username:</label>
                    <Input 
                      placeholder='Employee usernamename' 
                      value={this.state.username} 
                      onChange={this.handleChange('username')}/>
                </Form.Field>
                <Form.Field>
                    <label>Firstname:</label>
                    <Input 
                      placeholder='Employee firstname' 
                      value={this.state.firstname} 
                      onChange={this.handleChange('firstname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Secondname:</label>
                    <Input 
                      placeholder='Employee secondname' 
                      value={this.state.secondname} 
                      onChange={this.handleChange('secondname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Password:</label>
                    <Input 
                      placeholder='Employee password' 
                      type='password'
                      value={this.state.password} 
                      onChange={this.handleChange('password')}/>
                </Form.Field>
                <Form.Field>
                    <label>Email:</label>
                    <Input 
                      placeholder='Employee email' 
                      value={this.state.email} 
                      onChange={this.handleChange('email')}/>
                </Form.Field>
                <Form.Field>
                    <label>Phone number:</label>
                    <Input 
                      placeholder='Employee phone number' 
                      value={this.state.phoneNumber} 
                      onChange={this.handleChange('phoneNumber')}/>
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>this.addEmployee() } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}