import React, { Component } from 'react'
import { 
    Segment, 
    Header, 
    Grid
} from 'semantic-ui-react'
import axios from 'axios';
import _ from 'lodash'
import { 
    Image 
} from 'semantic-ui-react'
import { SemanticToastContainer,toast } from 'react-semantic-toasts'
import { PieChart, 
         Pie, 
         Sector, 
         BarChart, 
         Bar, 
         XAxis, 
         YAxis, 
         CartesianGrid, 
         Tooltip,
         Cell
        } from 'recharts'
import spinner from '../../../img/spinner.gif'

export default class Charts extends Component {
    state={
        categoryList: [],
        mapCategoryList: [],
        activeIndex: 0,
        productsList: [],
        colors: [],
        spinner: false
    }

    componentDidMount(){
        var self = this
        this.setState({
            spinner: true
        })
        this.getCategories()
        setTimeout( function() {
            self.getProducts()
        }, 200 )
    }

    getColors(){
        let colorsList = []
        let pom = 0
        while(pom < this.state.productsList.length){
            if(pom%2 === 1)
                colorsList.push("rgb("+(pom*10)%155 + ','+(pom*10)%155 + ','+(pom*10)%155 + ')')
            else
                colorsList.push("rgb("+(pom*10+200)%155 + ','+(pom*10+200)%155 + ','+(pom*10+200)%155 + ')')
            pom++
        }
        this.setState({
            colors: colorsList
        })
    }

    getCategories() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/category`)
            .then(function(response) {
                let categories = []
                let mapCategories = []
                response.data.map(category=>{
                    if(!category.parentId)
                    {
                        return (
                            categories.push({id: category.id, name: category.name, value: 0}),
                            mapCategories.push({id: category.id, parent: category.id}),
                            self.setState({
                                mapCategoryList: mapCategories
                            })
                        )
                    } else {
                        return(mapCategories.push({id: category.id, parent: self.findMainCategory({id: category.id, parent: category.parentId})}),
                        self.setState({
                            mapCategoryList: mapCategories
                        }))
                        }
                    })
                self.setState({
                    categoryList: categories
                })
            })
            .catch(function(error) {
                console.log(error);
			})
    }

    getProducts() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/product`)
            .then(function(response) {
                let categories = self.state.categoryList
                let products = []
                response.data.map(product=>{
                    return (
                        _.find(categories, {'id': 
                            _.find(self.state.mapCategoryList, {'id': product.categoryId}).parent})
                            .value += product.quantity,
                        products.push({id: product.id, name: product.name, value: product.quantity})
                    )
            })
            self.setState({
                categoryList: categories,
                productsList: products,
                spinner: false
            })
            self.getColors()
        })
            .catch(function(error) {
                console.log(error);
			})
    }

    findMainCategory(category) {
        console.log('lol', category)
        if(category && category.parent === category.id)
            return category.id
        else {
            return this.findMainCategory(_.find(this.state.mapCategoryList, {'id': category.parent}))
            }
    }

    onPieEnter(data, index) {
        this.setState({
          activeIndex: index
        })
      }

  render(){             
    const renderActiveShape = (props) => {
        const RADIAN = Math.PI / 180;
        const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
          fill, payload, percent, value } = props;
        const sin = Math.sin(-RADIAN * midAngle);
        const cos = Math.cos(-RADIAN * midAngle);
        const sx = cx + (outerRadius + 10) * cos;
        const sy = cy + (outerRadius + 10) * sin;
        const mx = cx + (outerRadius + 30) * cos;
        const my = cy + (outerRadius + 30) * sin;
        const ex = mx + (cos >= 0 ? 1 : -1) * 22;
        const ey = my;
        const textAnchor = cos >= 0 ? 'start' : 'end';
      
        return (
          <g>
            <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
            <Sector
              cx={cx}
              cy={cy}
              innerRadius={innerRadius}
              outerRadius={outerRadius}
              startAngle={startAngle}
              endAngle={endAngle}
              fill={fill}
            />
            <Sector
              cx={cx}
              cy={cy}
              startAngle={startAngle}
              endAngle={endAngle}
              innerRadius={outerRadius + 6}
              outerRadius={outerRadius + 10}
              fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
              {`(Rate ${(percent * 100).toFixed(2)}%)`}
            </text>
          </g>
        );
      }
      
      return(
        <Segment 
            style={{
                overflowY: 'scroll', 
                position: 'absolute', 
                left: this.props.isMenu ? '270px': '70px', 
                width: this.props.isMenu ? 'calc(100% - 280px)' : 'calc(100% - 70px)', 
                height: 'calc(100% - 75px)' }} 
                scroll
                >
        <Header as='h2' style={{position: 'sticky'}}>Charts</Header>
        <Grid>
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Header as='h3' style={{position: 'sticky'}}>Products quantity</Header>
                    <Segment style={{overflowX: 'scroll', height: 350}}>
                        {this.state.spinner ?<Image src={spinner} centered style={{ height: '240px'}}/>:
                        <BarChart 
                            width={this.state.productsList.length*120}
                            margin={{bottom: 100}}
                            height={250} 
                            data={this.state.productsList}
                                >
                            <CartesianGrid strokeDasharray="1 1"/>
                            <XAxis dataKey="name"
                                    interval={0}
                                    angle={-25}
                                    tick={{fontSize: 14}}
                                    textAnchor="end"/>
                            <YAxis/>
                            <Tooltip/>
                            <Bar dataKey="value" fill="#8884d8">
                            {
                                this.state.productsList.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={this.state.colors[index]}/>
                                ))
                            }
                            </Bar>
                        </BarChart>}
                    </Segment>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
                <Grid.Column>
                <Header as='h3' style={{position: 'sticky'}}>Main Categories quantity</Header>
                <Segment style={{ width: 700}}>
                    {this.state.spinner ?<Image src={spinner} centered style={{ height: '240px'}}/>:
                    <PieChart width={650} height={400}>
                        <Pie 
                            dataKey='value'
                            activeIndex={this.state.activeIndex}
                            activeShape={renderActiveShape} 
                            data={this.state.categoryList} 
                            cx={320} 
                            cy={200} 
                            innerRadius={80}
                            outerRadius={100} 
                            fill="rgb(100,100,100)"
                            onMouseEnter={this.onPieEnter.bind(this)}
                            />
                    </PieChart>}
                </Segment>    
                </Grid.Column>  
                <Grid.Column >
                    
                </Grid.Column>  
            </Grid.Row>
        </Grid>
        <SemanticToastContainer />
    </Segment>
      )
  }
}