Aby uruchomić aplikację na **frontendzie** należy:
1) wejść do katalogu frontend
2) zainstalować, jeśli nie mamy **Node** i **npm**
3) wpisać w konsoli **npm install**, aby zaintalować *node modules* (chyba, że już to wcześniej zrobiliśmy)
5) wpisać w konsoli **npm start**, a nasza aplikacja powinna sama uruchomić się w przeglądarce na porcie 3000


![diagram](/uploads/994ab4051e53a718cffcfdd33f2393ef/diagram.png)