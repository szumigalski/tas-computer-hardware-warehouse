﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.ViewModels.CategoryViewModels
{
    public class CategoryUpdateViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }
}
