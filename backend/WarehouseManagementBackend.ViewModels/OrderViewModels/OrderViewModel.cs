﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.ViewModels.OrderViewModels
{
    public class OrderViewModel
    {
        [Required]
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string userName { get; set; }
        public bool Status { get; set; }
    }
}
