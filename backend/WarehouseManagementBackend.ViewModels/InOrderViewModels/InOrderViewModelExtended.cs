﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.ViewModels.InOrderViewModels
{
    public class InOrderViewModelExtended
    {
        [Required]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int StatusId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string whoDid { get; set; }
    }
}
