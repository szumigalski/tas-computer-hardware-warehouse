﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.TagViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<TagViewModel, Tag>();
            CreateMap<Tag, TagViewModel>();

            CreateMap<TagUpdateViewModel, Tag>();
            CreateMap<Tag, TagUpdateViewModel>();
        }
    }
}
