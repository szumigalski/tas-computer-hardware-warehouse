﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.CategoryViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<CategoryViewModel, Category>();
            CreateMap<Category, CategoryViewModel>();

            CreateMap<CategoryUpdateViewModel, Category>();
            CreateMap<Category, CategoryUpdateViewModel>();
        }
    }
}
