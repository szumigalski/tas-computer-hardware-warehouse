﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class InOrderProfile : Profile
    {
        public InOrderProfile()
        {
            CreateMap<InOrderViewModelBasic, InOrder>();
            CreateMap<InOrder, InOrderViewModelBasic>();

            CreateMap<InOrderViewModelExtended, InOrder>();
            CreateMap<InOrder, InOrderViewModelExtended>();
        }
    }
}
