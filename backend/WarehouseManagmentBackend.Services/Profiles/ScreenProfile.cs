﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.ScreenViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class ScreenProfile : Profile
    {
        public ScreenProfile()
        {
            CreateMap<ScreenViewModel, Screen>();
            CreateMap<Screen, ScreenViewModel>();

            CreateMap<ScreenUpdateViewModel, Screen>();
            CreateMap<Screen, ScreenUpdateViewModel>();
        }
    }
}
