﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.CategoryViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class CategoryService : ServiceBase<CategoryViewModel, Category>, ICategoryService
    {
        public CategoryService(IMapper mapper, IRepository<Category> repository) : base(mapper,repository)
        {

        }

        public async Task DeleteWithChilds(int id)
        {
            var categoryChilds = Repository.Get(p => p.ParentId.Equals(id));
            foreach(var  element in categoryChilds)
            {
                if(Repository.Get(p => p.ParentId.Equals(element.Id)) == null)
                {
                    Repository.Delete(element);
                }
                else
                {
                    await DeleteWithChilds(element.Id);
                    Repository.Delete(element);
                }
            }
            await Delete(id);
        }
    }
}
