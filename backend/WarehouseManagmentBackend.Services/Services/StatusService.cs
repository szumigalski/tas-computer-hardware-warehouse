﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.StatusViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class StatusService : ServiceBase<StatusViewModel, Status>, IStatusService
    {
        public StatusService(IMapper mapper, IRepository<Status> repository) : base(mapper, repository)
        { }
    }
}
