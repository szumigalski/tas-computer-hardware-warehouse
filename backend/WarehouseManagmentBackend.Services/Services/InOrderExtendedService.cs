﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class InOrderExtendedService : ServiceBase<InOrderViewModelExtended, InOrder>, IInOrderExtendedService
    {
        public InOrderExtendedService(IMapper mapper, IRepository<InOrder> repository) : base(mapper, repository)
        { }

    }
}
