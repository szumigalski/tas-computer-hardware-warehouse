﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class ProductService : ServiceBase<ProductViewModel, Product>, IProductService
    {
        public ProductService(IMapper mapper, IRepository<Product> repository) : base(mapper, repository)
        { }

        public async Task<IEnumerable<Product>> GetProductsWithCategoryId(int id)
        {
            var products = Repository.Get(p => p.CategoryId.Equals(id));
            var result = Mapper.Map<IEnumerable<Product>>(products);
            return result;
        }
    }
}
