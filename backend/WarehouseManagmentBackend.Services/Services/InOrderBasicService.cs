﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class InOrderBasicService : ServiceBase<InOrderViewModelBasic, InOrder>, IInOrderBasicService
    {
        public InOrderBasicService(IMapper mapper, IRepository<InOrder> repository) : base(mapper,repository)
        {

        }

        public async Task CreateForEachElement(InOrderViewModelBasic element, int orderId)
        {
            element.StatusId = 1;
            element.OrderId = orderId;
            var result = Mapper.Map<InOrder>(element);
            await Repository.Create(result);
        }
    }
}
