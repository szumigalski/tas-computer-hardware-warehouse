﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.TagViewModels;

namespace WarehouseManagementBackend.Services.Services.Updates
{
    public class TagUpdateService : ServiceBase<TagUpdateViewModel, Tag>, ITagUpdateService
    {
        public TagUpdateService(IMapper mapper, IRepository<Tag> repository) : base(mapper, repository)
        {

        }
    }
}
