﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.CategoryViewModels;

namespace WarehouseManagementBackend.Services.Services.Updates
{
    public class CategoryUpdateService : ServiceBase<CategoryUpdateViewModel, Category>, ICategoryUpdateService
    {
        public CategoryUpdateService(IMapper mapper, IRepository<Category> repository) : base(mapper, repository)
        {

        }
    }
}
