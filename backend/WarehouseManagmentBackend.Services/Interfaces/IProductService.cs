﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Services.Interfaces
{
    public interface IProductService : IServiceBase<ProductViewModel, Product>
    {
        Task<IEnumerable<Product>> GetProductsWithCategoryId(int id);
    }
}
