﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Services.Interfaces.Updates
{
    public interface IProductUpdateService : IServiceBase<ProductUpdateViewModel, Product>
    {
    }
}
