﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.ScreenViewModels;

namespace WarehouseManagementBackend.Services.Interfaces.Updates
{
    public interface IScreenUpdateService : IServiceBase<ScreenUpdateViewModel, Screen>
    {
    }
}
