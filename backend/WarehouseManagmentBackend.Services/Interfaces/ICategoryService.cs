﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.CategoryViewModels;

namespace WarehouseManagementBackend.Services.Interfaces
{
    public interface ICategoryService : IServiceBase<CategoryViewModel, Category>
    {
        Task DeleteWithChilds(int id);
    }
}
