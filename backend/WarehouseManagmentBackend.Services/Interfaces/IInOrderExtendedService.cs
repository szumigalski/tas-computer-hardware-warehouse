﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;

namespace WarehouseManagementBackend.Services.Interfaces
{
    public interface IInOrderExtendedService : IServiceBase<InOrderViewModelExtended, InOrder>
    {
    }
}
