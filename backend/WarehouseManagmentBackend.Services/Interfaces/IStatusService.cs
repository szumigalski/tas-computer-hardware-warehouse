﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.StatusViewModels;

namespace WarehouseManagementBackend.Services.Interfaces
{
    public interface IStatusService : IServiceBase<StatusViewModel, Status>
    {
    }
}
