﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.ViewModels.TagViewModels;

namespace WarehouseManagementBackend.Services.Interfaces
{
    public interface ITagService : IServiceBase<TagViewModel, Tag>
    {
    }
}
