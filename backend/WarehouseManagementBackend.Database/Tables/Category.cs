﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        [ForeignKey(nameof(ParentCategory))]
        public int? ParentId { get; set; }
        public virtual Category ParentCategory { get; set; }
        public virtual ICollection<Category> CategoryChildren { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
