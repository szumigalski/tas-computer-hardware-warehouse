﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Screen : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Tag> Tags { get; set; } 
    }
}
