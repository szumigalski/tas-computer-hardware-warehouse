﻿using System;
using System.Collections.Generic;
using System.Text;
using static WarehouseManagementBackend.ViewModels.Enums.StatusName;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Status : BaseEntity
    {
        public StatusesNames StatusEnumNumber { get; set; }
        public string StatusName { get; set; }

        public virtual ICollection<InOrder> InOrders { get; set; }
    }
}
