﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Order : BaseEntity
    {
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool Status { get; set; }

        public string userName { get; set; }

        public virtual ICollection<InOrder> InOrders { get; set; }
    }
}
