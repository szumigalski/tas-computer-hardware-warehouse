﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class InOrderRepository : Repository<InOrder>, IRepository<InOrder>
    {
        public InOrderRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
