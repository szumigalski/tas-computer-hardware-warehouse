﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class OrderRepository : Repository<Order>, IRepository<Order>
    {
        public OrderRepository(ApplicationDbContext _dbContext)  : base(_dbContext)
        { }
    }
}
