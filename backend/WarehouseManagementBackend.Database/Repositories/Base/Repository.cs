﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories.Base
{
    public class Repository<T>: IRepository<T> where T : BaseEntity
    {
        protected ApplicationDbContext RepositoryContext { get; set; }

        public Repository(ApplicationDbContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }

        public IQueryable<T> Get()
        {
            return this.RepositoryContext.Set<T>();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> expression)
        {
            return this.RepositoryContext.Set<T>().Where(expression);
        }

        public Task<bool> Any(Expression<Func<T, bool>> expression)
        {
            return RepositoryContext.Set<T>().AnyAsync(expression);
        }

        public Task<bool> Any(int id)
        {
            return RepositoryContext.Set<T>().AnyAsync(n => n.Id == id);
        }


        public Task<T> Get(int id)
        {
            return RepositoryContext.Set<T>().FindAsync(id);
        }
        public virtual Task Create(T entity)
        {
            return Task.FromResult(RepositoryContext.Set<T>().AddAsync(entity));
        }

        public virtual Task Update(T entity)
        {
            return Task.FromResult(RepositoryContext.Set<T>().Update(entity));
        }

        public virtual void Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
        }

        public virtual async Task Save()
        {
            await RepositoryContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            RepositoryContext.Dispose();
        }
    }
}
