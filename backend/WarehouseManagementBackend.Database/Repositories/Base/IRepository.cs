﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories.Base
{
    /// <summary>
    /// Implementation of repository interface
    /// </summary>
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> Get();
        IQueryable<T> Get(Expression<Func<T, bool>> expression);
        Task<T> Get(int id);
        Task Create(T entity);
        Task Update(T entity);
        void Delete(T entity);
        Task<bool> Any(Expression<Func<T, bool>> expression);
        Task<bool> Any(int id);
        void Dispose();
        Task Save();
    }
}
