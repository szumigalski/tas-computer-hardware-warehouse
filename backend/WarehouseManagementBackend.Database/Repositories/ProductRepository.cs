﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class ProductRepository : Repository<Product>,IRepository<Product>
    {
        public ProductRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
