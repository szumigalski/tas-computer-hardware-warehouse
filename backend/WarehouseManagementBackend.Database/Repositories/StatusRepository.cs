﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class StatusRepository : Repository<Status>, IRepository<Status>
    {
        public StatusRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
