﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class CategoryRepository : Repository<Category>, IRepository<Category>
    {
        public CategoryRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
