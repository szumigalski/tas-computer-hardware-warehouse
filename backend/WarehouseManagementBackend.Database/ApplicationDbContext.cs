﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WarehouseManagementBackend.Database.Tables;
using static WarehouseManagementBackend.ViewModels.Enums.StatusName;

namespace WarehouseManagementBackend.Database
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Screen> Screens { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<InOrder> InOrders { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Product>().Property(p => p.Price).HasColumnType("decimal(18,2)");
            builder.Entity<Category>().HasData(
                new
                {
                    Id = 1,
                    Name = "Komputery",
                    IsProduct = false
                },
                new
                {
                    Id = 2,
                    Name = "Laptopy",
                    ParentId = 1,
                    IsProduct = false
                }
                );
            builder.Entity<Screen>().HasData(
                new
                {
                    Id = 1,
                    Name = "Widok1"
                },
                new
                {
                    Id = 2,
                    Name = "Widok2"
                }
                );
            builder.Entity<Tag>().HasData(
                new
                {
                    Id = 1,
                    Name = "Elektronika",
                    ScreenId = 1
                },
                new
                {
                    Id = 2,
                    Name = "Technika",
                    ScreenId = 1
                },
                new
                {
                    Id = 3,
                    Name = "Laptopki",
                    ScreenId =2
                }
                );
            builder.Entity<Product>().HasData(
                new
                {
                    Id = 1,
                    Name = "Laptop Y600",
                    Quantity = 4,
                    Price = 2314.9m,
                    CategoryId = 2,
                    TagId = 3
                },
                new
                {
                    Id = 2,
                    Name = "Zasilacz W222",
                    Quantity = 5,
                    Price = 4223.9m,
                    CategoryId = 1,
                    TagId = 2
                }
                );
            builder.Entity<Order>().HasData(
                new
                {
                    Id = 1,
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Today,
                    Status = true

                },
                new
                {
                    Id = 2,
                    StartTime = DateTime.Today,
                    EndTime = DateTime.Now,
                    Status = true
                }
                );
            builder.Entity<Status>().HasData(
                new
                {
                    Id = 1,
                    StatusEnumNumber = StatusesNames.Awaiting,
                    StatusName = StatusesNames.Awaiting.ToString()
                },
                new
                {
                    Id = 2,
                    StatusEnumNumber = StatusesNames.InProgress,
                    StatusName = StatusesNames.InProgress.ToString()
                },
                new
                {
                    Id = 3,
                    StatusEnumNumber = StatusesNames.Done,
                    StatusName = StatusesNames.Done.ToString()
                }
                );
            builder.Entity<InOrder>().HasData(
                new
                {
                    Id = 1,
                    OrderId = 1,
                    StatusId = 2,
                    ProductId = 1
                },
                new
                {
                    Id = 2,
                    OrderId = 2,
                    StatusId = 1,
                    ProductId = 2
                }
                );
        }
    }
}
