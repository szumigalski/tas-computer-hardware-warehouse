﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseManagementBackend.Database.Migrations
{
    public partial class PoprawaStatusow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 10, 29, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2018, 10, 29, 22, 16, 8, 461, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 10, 29, 22, 16, 8, 469, DateTimeKind.Local), new DateTime(2018, 10, 29, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Statuses",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "StatusEnumNumber", "StatusName" },
                values: new object[] { 1, "InProgress" });

            migrationBuilder.InsertData(
                table: "Statuses",
                columns: new[] { "Id", "StatusEnumNumber", "StatusName" },
                values: new object[] { 3, 2, "Done" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Statuses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 10, 25, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2018, 10, 25, 20, 27, 39, 323, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 10, 25, 20, 27, 39, 331, DateTimeKind.Local), new DateTime(2018, 10, 25, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Statuses",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "StatusEnumNumber", "StatusName" },
                values: new object[] { 2, "Done" });
        }
    }
}
