﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseManagementBackend.Database.Migrations
{
    public partial class Updatebazypoduwagifrontu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "userName",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "whoDid",
                table: "InOrders",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 11, 30, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2018, 11, 30, 20, 12, 27, 713, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 11, 30, 20, 12, 27, 725, DateTimeKind.Local), new DateTime(2018, 11, 30, 0, 0, 0, 0, DateTimeKind.Local) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "userName",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "whoDid",
                table: "InOrders");

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 11, 17, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2018, 11, 17, 22, 55, 4, 657, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "EndTime", "StartTime" },
                values: new object[] { new DateTime(2018, 11, 17, 22, 55, 4, 676, DateTimeKind.Local), new DateTime(2018, 11, 17, 0, 0, 0, 0, DateTimeKind.Local) });
        }
    }
}
