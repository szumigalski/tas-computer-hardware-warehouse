﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseManagementBackend.Database.Migrations
{
    public partial class AddSomeData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "IsProduct", "Name", "ParentId" },
                values: new object[] { 1, false, "Komputery", null });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "EndTime", "StartTime", "Status" },
                values: new object[,]
                {
                    { 1, new DateTime(2018, 10, 25, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2018, 10, 25, 20, 0, 41, 383, DateTimeKind.Local), true },
                    { 2, new DateTime(2018, 10, 25, 20, 0, 41, 387, DateTimeKind.Local), new DateTime(2018, 10, 25, 0, 0, 0, 0, DateTimeKind.Local), true }
                });

            migrationBuilder.InsertData(
                table: "Screens",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Widok1" },
                    { 2, "Widok2" }
                });

            migrationBuilder.InsertData(
                table: "Statuses",
                columns: new[] { "Id", "StatusEnumNumber", "StatusName" },
                values: new object[,]
                {
                    { 1, 0, "Awaiting" },
                    { 2, 2, "Done" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "IsProduct", "Name", "ParentId" },
                values: new object[] { 2, false, "Laptopy", 1 });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Name", "ScreenId" },
                values: new object[,]
                {
                    { 1, "Elektronika", 1 },
                    { 2, "Teechnika", 1 },
                    { 3, "Laptopki", 2 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Name", "Price", "Quantity", "TagId" },
                values: new object[] { 2, 1, "Zasilacz W222", 4223.9m, 5, 2 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Name", "Price", "Quantity", "TagId" },
                values: new object[] { 1, 2, "Laptop Y600", 2314.9m, 4, 3 });

            migrationBuilder.InsertData(
                table: "InOrders",
                columns: new[] { "Id", "OrderId", "ProductId", "StatusId" },
                values: new object[] { 2, 2, 2, 1 });

            migrationBuilder.InsertData(
                table: "InOrders",
                columns: new[] { "Id", "OrderId", "ProductId", "StatusId" },
                values: new object[] { 1, 1, 1, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "InOrders",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "InOrders",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Statuses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Statuses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Screens",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Screens",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
