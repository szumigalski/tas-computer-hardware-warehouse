﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Services;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.Services.Services.Updates;

namespace WarehouseManagementBackend.Configurations
{
    public static class ServiceConfiguration
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IInOrderBasicService, InOrderBasicService>();
            services.AddScoped<IInOrderExtendedService, InOrderExtendedService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IScreenService, ScreenService>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IStatusService, StatusService>();
            services.AddScoped<ICategoryUpdateService, CategoryUpdateService>();
            services.AddScoped<IProductUpdateService, ProductUpdateService>();
            services.AddScoped<IScreenUpdateService, ScreenUpdateService>();
            services.AddScoped<ITagUpdateService, TagUpdateService>();

            return services;
        }
    }
}
