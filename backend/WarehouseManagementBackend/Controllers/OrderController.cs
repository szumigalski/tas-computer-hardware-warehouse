﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;
using WarehouseManagementBackend.ViewModels.OrderViewModels;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/order")]
    public class OrderController : Controller
    {
        private readonly IOrderService _service;
        private readonly IInOrderBasicService _inOrderBasicService;
        private readonly IProductUpdateService _updateService;
        private readonly IProductService _productService;

        public OrderController(IOrderService service, IInOrderBasicService inOrderBasicService, IProductUpdateService updateService, IProductService productService)
        {
            _service = service;
            _inOrderBasicService = inOrderBasicService;
            _updateService = updateService;
            _productService = productService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpGet("user")]
        public async Task<IActionResult> GetForUser(string username)
        {
            return Json( _service.Get().Where( p => p.userName.Equals(username)));
        }

        [HttpGet("create")]
        public async Task<IActionResult> Create()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var order = _service.Create();
            await _service.SaveAsync();
            
            return Json(order.Result.Id);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateInOrder(int orderID, int productID, string productName)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            InOrderViewModelBasic element = new InOrderViewModelBasic { ProductId = productID };
            
            await _inOrderBasicService.CreateForEachElement(element, orderID);
            await _inOrderBasicService.SaveAsync();
            
            

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(OrderViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Update(model.Id, model);
            await _service.SaveAsync();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();

            return Ok();
        }
    }
}