﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.ScreenViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/workspace")]
    public class ScreenController : Controller
    {
        private readonly IScreenService _service;
        private readonly IScreenUpdateService _updateService;

        public ScreenController(IScreenService service, IScreenUpdateService updateService)
        {
            _service = service;
            _updateService = updateService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(ScreenViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _service.Create(model);
            await _service.SaveAsync();
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(ScreenUpdateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _updateService.Update(model.Id, model);
            await _updateService.SaveAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();
            return Ok();
        }
    }
}
