﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.InOrderViewModels;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/inorderextended")]
    public class InOrderExtendedController : Controller
    {
        private readonly IInOrderExtendedService _service;
        private readonly IProductUpdateService _updateService;
        private readonly IProductService _productService;

        public InOrderExtendedController(IInOrderExtendedService service, IProductUpdateService updateService, IProductService productService)
        {
            _service = service;
            _updateService = updateService;
            _productService = productService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(InOrderViewModelExtended model)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var productData =await _productService.Get(model.ProductId);
            productData.Quantity -= 1;
            ProductUpdateViewModel updatedQuantity = new ProductUpdateViewModel { Quantity = productData.Quantity - 1};
            await _updateService.Update(productData.Id, updatedQuantity);
            await _service.Create(model);
            await _service.SaveAsync();
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(InOrderViewModelExtended model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Update(model.Id, model);
            await _service.SaveAsync();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();

            return Ok();
        }
    }
}