import { createAppContainer, createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import Login from './components/Login'
import AdminPanel from './components/AdminPanel/AdminPanel'

const AppNavigator = createStackNavigator({
  Login: {
    screen: Login,
  },
  AdminPanel: {
    screen: AdminPanel,
  }
}, {
    initialRouteName: 'Login',
});

export default createAppContainer(AppNavigator);