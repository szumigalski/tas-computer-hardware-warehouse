import React, { Component } from 'react'
import {create} from 'apisauce'
import { StyleSheet, ListView} from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Text } from 'native-base';

export default class Screens extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            basic: true,
            screenList: []
        }
    }
    componentDidMount(){
        this.getScreens()
    }

    getScreens(){
        var self = this
        const api = create({
            baseURL: 'https://waremana.projektstudencki.pl',
            headers: {'Accept': 'application/vnd.github.v3+json'}
          })
        api
          .get('/api/workspace')
          .then((response) => {
              let screens =[]
              response.data.map(screen=>{
                screens.push(
                    {
                        id: screen.id, 
                        name: screen.name,
                        value: screen.name, 
                        text: screen.name,
                        show: true 
                    }
                )
              })
              self.setState({
                screenList: screens
            })
            }) 
    }

    render(){  
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return (
            <Container>
                <Content>
                <List
                    leftOpenValue={75}
                    rightOpenValue={-75}
                    dataSource={this.ds.cloneWithRows(this.state.screenList)}
                    renderRow={data =>
                    <ListItem >
                        <Text > {data.name} </Text>
                    </ListItem>}
                    renderLeftHiddenRow={data =>
                    <Button full onPress={() => alert(data)}>
                        <Icon active name="information-circle" />
                    </Button>}
                    renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button full danger onPress={() => alert(data)}>
                        <Icon active name="trash" />
                    </Button>}
                />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
  headcell: {
    width: '50%'
  }
});