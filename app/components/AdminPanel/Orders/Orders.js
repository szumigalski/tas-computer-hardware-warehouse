import React, { Component } from 'react'
import {create} from 'apisauce'
import { StyleSheet, ListView} from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Text } from 'native-base';

export default class Orders extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            basic: true,
            orderList: []
        }
    }
    componentDidMount(){
        this.getOrders()
    }

    getOrders(){
        var self = this
        const api = create({
            baseURL: 'https://waremana.projektstudencki.pl',
            headers: {'Accept': 'application/vnd.github.v3+json'}
          })
        api
          .get('/api/order')
          .then((response) => {
              let orders =[]
              response.data.map(order=>{
                orders.push(
                    {
                        id: order.id,
                        startTime: order.startTime,
                        endTime: order.endTime,
                        status: order.status,
                    }
                )
              })
              self.setState({
                orderList: orders
            })
            }) 
    }
    timeConverter(time){
        var a = new Date(time);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        time = date + ' ' + month + ' ' + year;
        return time;
      }
    render(){  
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return (
            <Container>
                <Content>
                <List
                    leftOpenValue={75}
                    rightOpenValue={-75}
                    dataSource={this.ds.cloneWithRows(this.state.orderList)}
                    renderRow={data =>
                    <ListItem style={{display: 'flex', justifyContent: 'center'}}>
                        <Text > {this.timeConverter(data.startTime)} </Text>
                        <Text > {this.timeConverter(data.endTime)} </Text>
                        <Text > {data.status} </Text>

                    </ListItem>}
                />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
  headcell: {
    width: '50%'
  }
});