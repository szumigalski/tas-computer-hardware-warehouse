import React, { Component } from 'react'
import { 
    Container, 
    Tab, 
    Tabs,
    TabHeading,
    Icon
     } from 'native-base';
import Products from './Products/Products';
import Tags from './Tags/Tags'
import Orders from './Orders/Orders'
import Screens from './Screens/Screens'

export default class AdminPanel extends Component {
    static navigationOptions = {
        title: 'Computer Hardware Warehouse'
      }
    render(){
        return(
            <Container>
                <Tabs locked>
                    <Tab heading={<TabHeading><Icon name="folder" /></TabHeading>}>
                        <Products />
                    </Tab>
                    <Tab heading={<TabHeading><Icon name="md-cart" /></TabHeading>}>
                        <Orders />
                    </Tab>
                    <Tab heading={<TabHeading><Icon name="md-attach" /></TabHeading>}>
                        <Tags />
                    </Tab>
                    <Tab heading={<TabHeading><Icon name="ios-card" /></TabHeading>}>
                        <Screens />
                    </Tab>
                    
                </Tabs>
            </Container>
        )
    }
}